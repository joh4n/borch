"""
tests
"""
import unittest

import torch

import borch.distributions as dist
from borch.tensor import RandomVariable
from borch.guide import PointMassGuide
from borch import Module
from borch.infer.model_conversion import model_to_neg_log_prob_closure


class Test_model_to_neg_log_prob_closure(unittest.TestCase):
    def test_model_to_neg_log_prob_closure(self):
        def forward(latent):
            latent.weight1 = RandomVariable(dist.Gamma(0.5, 0.5 / 2))
            latent.weight2 = RandomVariable(dist.Normal(loc=1, scale=2))

        model = Module()

        def model_call():
            forward(model)

        trace = PointMassGuide()
        model.guide = trace
        closure = model_to_neg_log_prob_closure(model_call, model)
        self.assertTrue(isinstance(closure(), torch.Tensor))


if __name__ == "__main__":
    unittest.main()
