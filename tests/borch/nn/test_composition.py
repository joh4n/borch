from unittest import TestCase
import torch
from borch import nn
from borch.nn.compose import apply_sequentially


class Test_Concurrent(TestCase):
    def setUp(self):
        self.x = torch.randn(3, 5)

    def test_outputs_tuple(self):
        split = nn.Concurrent(nn.ReLU(), nn.ReLU())
        out = split(self.x)
        self.assertIsInstance(out, tuple)

    def test_with_linear_has_right_shapes(self):
        split = nn.Concurrent(nn.Linear(5, 10), nn.Identity())
        out = split(self.x)
        self.assertEqual(out[0].shape, torch.Size([3, 10]))
        self.assertEqual(out[1].shape, torch.Size([3, 5]))

    def test_works_with_sequential(self):
        fn_module = nn.Apply(lambda x: torch.cat((x[0], x[1])))
        seq = nn.Sequential(nn.Concurrent(nn.Identity(), fn_module))
        y = torch.randn(3, 5)

        out = seq((self.x, y))

        self.assertTrue(torch.equal(out[0][0], self.x))
        self.assertTrue(torch.equal(out[0][1], y))
        self.assertTrue(torch.equal(out[1], torch.cat((self.x, y))))


class Test_Select(TestCase):
    def setUp(self):
        self.x_0 = torch.randn(3, 5)
        self.x_1 = torch.randn(3, 5)
        self.x = (self.x_0, self.x_1)

    def test_outputs_tuple(self):
        selector = nn.Select(0, 1)
        out = selector(self.x)
        self.assertIsInstance(out, tuple)

    def test_can_select_more_than_once(self):
        selector = nn.Select(0, 0, 1, 1)
        out = selector(self.x)
        self.assertEqual(len(out), 4)

    def test_selected_twice_is_equal(self):
        selector = nn.Select(0, 0)
        (out0, out1) = selector(self.x)
        self.assertIs(out0, out1)

    def test_nested_indexation(self):
        select = nn.Select(0, [1, 0])
        x = (torch.ones(1), (torch.ones(2), torch.ones(3, 3)))
        out = select(x)
        self.assertIs(out[0], x[0])
        self.assertIs(out[1], x[1][0])

        select = nn.Select([1, 1])
        out = select(x)
        self.assertIs(out[0], x[1][1])


class Network(nn.Sequential):
    def __init__(self):
        super().__init__()
        self.one = nn.ReLU()
        self.two = nn.ReLU()
        self.three = nn.Sequential(*[nn.ReLU() for _ in range(5)])


class TestSequential(TestCase):
    def setUp(self):
        self.net = Network()

    def test_slice(self):
        inital_len = len(self.net)
        slice_net = self.net[:-1]
        self.assertEqual(len(slice_net), inital_len - 1)

    def test_slice_is_sequential(self):
        self.assertIsInstance(self.net[1:], nn.Sequential)
        self.assertFalse(isinstance(self.net[1:], Network))

    def test_getitem(self):
        self.assertIs(self.net[0], list(self.net._modules.values())[0])

    def test_forward_with_tuple_output(self):
        # NB any module output which is a list or tuple is automatically
        # expanded in the forward pass of the Sequential.
        # pylint: disable=arguments-differ,no-self-use

        class ModuleA(nn.Module):
            def forward(self, _):
                return 1, 2

        class ModuleB(nn.Module):
            def forward(self, x, y):
                return x + y

        net = nn.Sequential(ModuleA(), ModuleB())
        _ = net(5)

    @staticmethod
    def test_var_args_to_forward():
        class ModuleThatForwardsTuple(nn.Module):
            @staticmethod
            def forward(*x):
                return x

        sequential_that_forwards_tuple = nn.Sequential(
            ModuleThatForwardsTuple(), nn.Sequential()
        )
        x = (0, 0, 0)
        sequential_that_forwards_tuple(x)

    def test_inherit_torch_sequential(self):
        self.assertIsInstance(self.net, nn.Sequential)

    def test_str_indexing_throws_error(self):
        with self.assertRaises(TypeError):
            self.net["one"]  # pylint: disable=pointless-statement

    def test_single_tensor_to_forward(self):
        x = torch.zeros(5, 5)

        sequential_with_sequentials = nn.Sequential(nn.Sequential(), nn.Sequential())

        x = sequential_with_sequentials(x)
        self.assertEqual(1, len(x))
        self.assertTrue(torch.equal(x[0], torch.zeros(5, 5)))

    def test_linear_modules_in_sequential(self):
        x = torch.zeros(5)
        sequential_with_linears = nn.Sequential(
            nn.Linear(5, 6, bias=False), nn.Linear(6, 7, bias=False)
        )
        x = sequential_with_linears(x)
        self.assertTrue(torch.equal(x, torch.zeros(7)))


class Test_apply_sequentially(TestCase):
    def setUp(self):
        self.test_img = torch.randn(1, 3, 20, 20)
        self.mod1 = nn.Conv2d(3, 5, 3)
        self.mod2 = nn.Conv2d(5, 9, 3)
        self.mod3 = nn.Conv2d(9, 3, 3)
        self.modules = [self.mod1, self.mod2, self.mod3]

    def test_same_result_as_manually(self):
        out1 = apply_sequentially(self.test_img, self.modules)
        out2 = self.mod1(self.test_img)
        out2 = self.mod2(out2)
        out2 = self.mod3(out2)
        self.assertTrue(torch.all(torch.eq(out1, out2)))
