# Utility functions
The functions are separated into sub modules with minimal dependencies.

## NOTE:
The utils package is not allowed to include imports from other parts of borch. Any
function that depends on an import from `borch.*.**` where * is not utils should be
placed in an appropriate `borch.*.utils` file.