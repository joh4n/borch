from copy import deepcopy
from io import BytesIO
import unittest
from itertools import chain
import pickle as pkl

import torch

from borch import TransformedParameter
from borch.distributions.transformations import IdentityTransform


class TestTransformedParameter(unittest.TestCase):
    def setUp(self):
        self.shape = (3,)
        self.u_tensor = torch.ones(self.shape, requires_grad=True)
        self.transform = torch.exp
        self.param = TransformedParameter(self.transform, self.u_tensor)

    def test_repr(self):
        prefix = "TransformedParameter containing:\n"
        self.assertTrue(repr(self.param).startswith(prefix))

    def test_parameters_returns_generator(self):
        gen = (1 for _ in range(3))
        self.assertEqual(type(self.param.parameters()), type(gen))

    def test_parameters_returns_tensor_when_grad_required(self):
        self.assertEqual(len(tuple(self.param.parameters())), 1)

    def test_parameters_return_tensor_when_not_leaf(self):
        param = TransformedParameter(self.transform, self.param * 4)
        self.assertEqual(len(tuple(param.parameters())), 1)

    def test_unique_objects(self):
        self.assertNotEqual(id(self.param), id(self.u_tensor))

    def test_transformation_applied_at_init(self):
        self.assertTrue(torch.equal(self.param, self.transform(self.u_tensor)))

    def test_gradients_propagate_correctly(self):
        (self.param * 3).sum().backward()
        self.assertIsNotNone(self.u_tensor.grad)

    def test_raises_value_error_if_u_tensor_is_not_tensor(self):
        with self.assertRaises(ValueError):
            TransformedParameter(self.transform, 3)

    def test_raises_value_error_if_transformation_is_not_callable(self):
        with self.assertRaises(ValueError):
            TransformedParameter(3, self.u_tensor)

    def test_gradients_propagate_through_a_distribution(self):
        mu = torch.ones(self.shape)
        dist = torch.distributions.Normal(mu, self.param)
        (dist.rsample() * 3).sum().backward()
        self.assertIsNotNone(self.u_tensor.grad)

    def test_self_and_all_parameters_are_cast_to_double_when_applied(self):
        self.param.double()
        for param in chain(self.param, self.param.parameters()):
            self.assertIs(param.dtype, torch.float64)

    def test_identity_transform(self):
        value = torch.tensor(torch.ones(1), requires_grad=True)
        transform = IdentityTransform()
        par = TransformedParameter(transform, value)
        self.assertEqual(id(value), id(par.u_tensor))
        self.assertIsNotNone(par.parameters())
        self.assertNotEqual(id(par), id(value))
        self.assertEqual(par, transform(value))

    def test_same_value_after_pickle(self):
        param = TransformedParameter(torch.exp, torch.tensor([1.0]))
        f = BytesIO()
        pkl.dump(param, f)
        f.seek(0)
        pickled_param = pkl.load(f)
        self.assertEqual(pickled_param, param)

    def test_several_backward(self):
        mu = torch.ones(1, requires_grad=True)
        t_par = TransformedParameter(torch.exp, mu)

        t_par = t_par.expand(torch.Size([2]))
        t_par.new()
        t_par._recalculate_transformation()
        loss = (t_par * 2).sum()
        loss.backward()
        t_par._recalculate_transformation()
        loss = (t_par * 2).sum()
        loss.backward()
        self.assertTrue(mu.grad is not None)

    def test_several_recalc_used_before_backward(self):
        ## broken for pytorch 1.0.0 as the copy_() breaks for mul oppertions,
        ## one need to create a new node first

        # mu = torch.ones(1, requires_grad=True)
        # t_par = TransformedParameter(torch.exp, mu)
        # loss = 0
        # for _ in range(3):
        #     t_par.recalculate()
        #     loss += (t_par * 2).sum()
        # loss.backward()
        # self.assertTrue(mu.grad is not None)
        pass

    def test_parameters_tensors_all_tensors_returned(self):
        self.assertEqual(len([self.param.parameters()]), 1)

    def test_parameters_tensors_returns_correct_tensor(self):
        self.assertEqual(id(list(self.param.parameters())[0]), id(self.u_tensor))

    def test_is_transformation_is_same_after_pickling(self):
        param = pkl.loads(pkl.dumps(self.param))
        self.assertIs(self.param.transformation, param.transformation)

    def test_is_still_transformed_parameter_after_pickling(self):
        f = BytesIO()
        pkl.dump(self.param, f)
        f.seek(0)
        pickled_param = pkl.load(f)
        self.assertIsInstance(pickled_param, TransformedParameter)

    def test_changing_values_in_tensor_after_pickling_doesnt_effect_other(self):
        param = pkl.loads(pkl.dumps(self.param))
        self.param *= 2
        self.assertEqual(self.param.sum() / param.sum(), 2)

    def test_one_reference_to_utensor(self):
        self.assertIs(self.param.u_tensor, self.param._parameters["u_tensor"])

    def test_reference_point_to_same_after_deepcopy(self):
        self.param.u_tensor = deepcopy(self.param.u_tensor)
        self.assertIs(self.param.u_tensor, self.param._parameters["u_tensor"])
