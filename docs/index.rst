.. borch documentation master file, created by
   sphinx-quickstart on Sat May 12 15:57:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to borch's documentation!
=================================

For development and installation guide lines please read trough
the project information links. It covers installation procedure,
development workflow etc.


For examples of how to use borch see the Tutorials section.



.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Project Information

   notes/README.md
   notes/CONTRIBUTING.md
   notes/CODE_OF_CONDUCT.md

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Tutorials

   tutorials/index.rst
   examples/*

.. toctree::
   :maxdepth: 2
   :caption: Package Reference

   api/borch
   api/borch.guide
   api/borch.module
   api/borch.module_base
   api/borch.optimizer_collection
   api/borch.distributions
   api/borch.infer
   api/borch.nn
   api/borch.utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

