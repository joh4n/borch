"""
Introduction to Borch
=======================
"""
##########################################
# Borch's universal borch allows the creation of probabilistic models with arbitrary
# control flow. The core components of the borch are ``borch.RandomVariable`` and
# ``borch.nn.Module``.
#
# Lets start of with the imports:
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import torch

import borch
from borch import infer
import borch.distributions as dist

####################################################
# RandomVariable
# ---------------
# The `borch.RandomVariable` merges a ``torch.distributions.Distribution`` and a
# ``torch.tensor``, it acts like a tensor and can be used just like one,
# but it also support methods such as ``.log_prob()``, ``.entropy()``, ``.sample()``,
# ``.rsample()`` etc.
#
# The ``borch.RandomVariable`` is initialized with a
# ``borch.borch.distributions.Distribution`` and it is this object that is being used
# for the ``.log_prob()``, ``.entropy()``,
# ``.sample()``, ``.rsample()`` etc. methods.

rvar = borch.RandomVariable(dist.Normal(0, 1))
print(rvar)
##################################################
# It can be used just like a normal tensor
print(rvar * 100)
print(rvar * torch.randn(10))

###################################################
# The distribution that the ``borch.RandomVariable`` is initialized with, is accessible
# in the field ``.distribution``. The method on the ``borch.RandomVariable`` differs
# sightly form that of a ``borch.distributions.Distribution`` in that feeds in its
# own tensor as the input if no args are provided.
print(rvar.log_prob())
rvar.log_prob() == rvar.log_prob(rvar)
print(rvar.log_prob(torch.zeros(1)))

##################################################
# It also supports sampling(`.sample()`) and reparameterized sampeling(`.rsample()`)
# if available.
rvar.sample()
print(rvar)

plt.hist([rvar.sample().item() for i in range(1000)])


################################################
# #Observing | conditioning
# In order to observe/condtion on a specific value, it is possible to call the
# ``.observe()`` method on the ``RandomVariable``. This will assign the specified
# value as the value of the ``RandomVariable`` and ``.sample()`` and ``.rsample()`` will
#  not over write the value, but return it.
obs = borch.RandomVariable(dist.Normal(1, 1)).observe(torch.ones(1))
print(obs)
print(obs.sample())


################################################
# Module
# ---------------
# The ``borch.nn.Module`` is an object that supports attaching and book keeping of
# ``borch.RandomVariable``'s. It also got a guide that specifies how the approximating
# distribution look. It is the recommended practice to write models as functions that
# have a ``borch.nn.Module`` as a first argument.


def forward(module):
    module.weight1 = borch.RandomVariable(dist.Gamma(1, 1 / 2))
    module.weight2 = borch.RandomVariable(dist.Normal(loc=1, scale=2))
    module.weight3 = borch.RandomVariable(dist.Normal(loc=1, scale=2))
    mu = module.weight1 + module.weight2 + module.weight3
    module.obs = borch.RandomVariable(dist.Normal(mu, 1)).observe(torch.ones(1))
    return mu


#####################################################################
# By feeding in a ``borch.nn.Module`` to the model function, the weights will be added
# to the ``borch.nn.Module`` object in place. The method ``module.pq_to_infer()``
# converts the RandomVariables that are attached to the Model object into a dict with
# lists, that contains p_dist, q_dist and value that can be used in the infer package.
#
# In order to access all the parameters that we want to optimize, we run trough the
# model once before creating the optimizer.

module = borch.Module()
module.sample()
forward(module)
optimizer = torch.optim.Adam(module.opt_parameters())

#####################################
# Fitting a model using the infer package looks like this:
for ii in range(10):
    optimizer.zero_grad()
    module.sample()
    forward(module)
    loss = infer.vi_loss(**module.pq_to_infer())
    loss.backward()
    optimizer.step()


################################################################
# Then the fitted ``borch.nn.Module`` object can be used to generate samples. Simply by
# using it in the model function i.e.
for _ in range(5):
    print(forward(module))


#################################################################
# Condition
# ---------------------
# For convince it would be better to specify what to condition(observe) on
# after the model is created and also allowing one to switch what to condition on.
# this can be done with
module.observe(weight2=torch.zeros(1), obs=torch.ones(1))

##################################################################
# It will continue to condition on the values until others are provided,
# one can stop all conditioning by specifying
module.observe(None)


########################################################################
# Exercises
# ----------
# 1) Create three ``RandomVariable``'s one with a discrete distribution, a continuous
#    distribution that is unconstrained and a continuous distribution that has a
#    positive support. Test the different methods on them like:
#
#    - ``.samlpe()``
#    - ``.rsamlpe()``
#    - ``.log_prob()``
#    - ``.entropy()``
