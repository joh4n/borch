from unittest import TestCase

import torch

from borch.nn.activations.activation_functions import Hswish, Hsigmoid


class TestHswish(TestCase):
    def setUp(self):
        self.x = torch.tensor([-1.0, 0.0, 1.0], requires_grad=True)
        self.hswish = Hswish()

    def test_forward(self):
        activation = self.hswish(self.x)
        expected = torch.Tensor([-0.33333333, 0.0, 0.66666666])
        self.assertTrue(torch.all(torch.eq(activation, expected)))

    def test_backward(self):
        activation = self.hswish(self.x)
        expected_grad = torch.Tensor([0.1666666666, 0.5, 0.833333373069763183593750])
        activation.sum().backward()
        self.assertTrue(torch.all(torch.eq(self.x.grad, expected_grad)))


class TestHsigmoid(TestCase):
    def setUp(self):
        self.x = torch.tensor([-1.0, 0.0, 1.0], requires_grad=True)
        self.hsigmoid = Hsigmoid()

    def test_forward(self):
        x = torch.Tensor([-1.0, 0.0, 1.0])
        hsigmoid = Hsigmoid()
        activation = hsigmoid(x)
        expected = torch.Tensor([0.33333333, 0.5, 0.66666666])
        self.assertTrue(torch.all(torch.eq(activation, expected)))

    def test_backward(self):
        activation = self.hsigmoid(self.x)
        expected_grad = torch.Tensor(
            [0.16666667163372, 0.16666667163372, 0.16666667163372]
        )
        activation.sum().backward()
        self.assertTrue(torch.all(torch.eq(self.x.grad, expected_grad)))
