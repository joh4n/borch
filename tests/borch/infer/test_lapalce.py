from unittest import TestCase
from io import BytesIO
import pickle
import torch
from torch import optim
import numpy.testing as npt
from borch.infer import laplace
from borch import nn, infer, guide
from borch import distributions as dist, RV


class Net(nn.Module):
    def __init__(self):
        super().__init__(guide=guide.DeltaGuide())
        self.rv = RV(dist.Normal(torch.ones(2), 0.5 * torch.ones(2)))
        self.rv2 = RV(dist.Normal(torch.ones(2), 0.5 * torch.ones(2)))

    def forward(self):  # pylint: disable=arguments-differ
        out = self.rv + self.rv2
        return out


def create_opt_closure(net, opt):
    def closure():
        opt.zero_grad()
        net.sample()
        net()
        loss = infer.vi_loss(**net.pq_to_infer(), kl_scaling=1)
        loss.backward()
        return loss

    return closure


def create_lapalce_closure(net):
    def closure():
        net.sample()
        net()
        loss = infer.vi_loss(**net.pq_to_infer(), kl_scaling=1)
        return loss

    return closure


class Test_laplace(TestCase):
    def setUp(self):
        net = Net()
        net()
        opt = optim.LBFGS(net.parameters())
        for _ in range(10):
            opt.step(create_opt_closure(net, opt))

        self.params = list(net.opt_parameters())
        self.fit = laplace.Laplace(create_lapalce_closure(net), net.opt_parameters())

    def test_correct_cov_mat(self):
        npt.assert_almost_equal(
            self.fit.dist.covariance_matrix.detach().numpy(), torch.eye(4).numpy() * 0.5
        )

    def test_rsample_updates_params(self):
        par_0 = float(self.params[0][0])
        self.fit.sample()
        self.assertNotEqual(par_0, float(self.params[0][0]))

    def test_params_are_leaf_nodes(self):
        for par in self.params:
            self.assertTrue(par.is_leaf)

    def test_to(self):
        self.fit.to(torch.float64)
        self.assertEqual(self.fit.dist.dtype, torch.float64)

    def test_pickle_and_unpickle(self):
        f = BytesIO()
        pickle.dump((self.fit, self.params), f)
        f.seek(0)
        del self.fit
        fit, params = pickle.load(f)
        par_0 = float(params[0][0])
        fit.sample()
        self.assertNotEqual(par_0, float(params[0][0]))
