from unittest import TestCase
from itertools import product
from numpy import testing as npt

# pylint: disable=unused-wildcard-import,wildcard-import,too-many-locals
from torch.autograd import grad


from borch.distributions.transforms import *
import borch.distributions as dist
from borch.distributions import constraints, transforms


class Test_init_Transform_base(TestCase):
    def test_non_valid_cach_size_raises_value_error(self):
        with self.assertRaises(ValueError):
            transforms.Transform(10)

    def test_equality(self):
        trans = transforms.Transform(0)
        trans._inv = 1
        self.assertTrue(trans == trans)  # pylint: disable=comparison-with-itself


class TestTransforms(TestCase):
    def setUp(self):
        super(TestTransforms, self).setUp()
        self.transforms = []
        for cache_size in [0, 1]:
            trans = [
                AbsTransform(cache_size=cache_size),
                ExpTransform(cache_size=cache_size),
                PowerTransform(exponent=2, cache_size=cache_size),
                PowerTransform(
                    exponent=torch.tensor(5.0).normal_().abs().clamp(min=0.1),
                    cache_size=cache_size,
                ),
                SigmoidTransform(cache_size=cache_size),
                AffineTransform(0, 1, cache_size=cache_size),
                AffineTransform(1, -2, cache_size=cache_size),
                AffineTransform(1, 2, event_dim=1, cache_size=cache_size),
                AffineTransform(torch.randn(5), torch.randn(5), cache_size=cache_size),
                AffineTransform(
                    torch.randn(4, 5), torch.randn(4, 5), cache_size=cache_size
                ),
                SoftmaxTransform(cache_size=cache_size),
                StickBreakingTransform(cache_size=cache_size),
                LowerCholeskyTransform(cache_size=cache_size),
                ComposeTransform(
                    [
                        AffineTransform(
                            torch.randn(4, 5), torch.randn(4, 5), cache_size=cache_size
                        )
                    ]
                ),
                ComposeTransform(
                    [
                        AffineTransform(
                            torch.randn(4, 5), torch.randn(4, 5), cache_size=cache_size
                        ),
                        ExpTransform(cache_size=cache_size),
                    ]
                ),
                ComposeTransform(
                    [
                        AffineTransform(0, 1, cache_size=cache_size),
                        AffineTransform(
                            torch.randn(4, 5), torch.randn(4, 5), cache_size=cache_size
                        ),
                        AffineTransform(1, -2, cache_size=cache_size),
                        AffineTransform(
                            torch.randn(4, 5), torch.randn(4, 5), cache_size=cache_size
                        ),
                    ]
                ),
            ]
            for transform in trans[:]:
                trans.append(transform.inv)
            trans.append(IdentityTransform())
            self.transforms += trans
            if cache_size == 0:
                self.unique_transforms = trans[:]

    @staticmethod
    def _generate_data(transform):
        domain = transform.domain
        codomain = transform.codomain
        x = torch.empty(4, 5)
        if (
            domain is constraints.lower_cholesky
            or codomain is constraints.lower_cholesky
        ):
            x = torch.empty(6, 6)
            x = x.normal_()
            return x
        if domain is constraints.real:
            return x.normal_()
        if domain is constraints.positive:
            return x.normal_().exp()
        if domain is constraints.unit_interval:
            return x.uniform_()
        if domain is constraints.simplex:
            x = x.normal_().exp()
            x /= x.sum(-1, True)
            return x
        raise ValueError("Unsupported domain: {}".format(domain))

    def test_inv_inv(self):
        for transform in self.transforms:
            self.assertTrue(transform.inv.inv == transform)

    def test_bijective_returns_bool(self):
        for transform in self.transforms:
            self.assertIsInstance(transform.bijective, bool)

    def test_inv_sign_type(self):
        for transform in self.transforms:
            try:
                self.assertIsInstance(
                    transform.inv.sign, (numbers.Number, torch.Tensor)
                )
            except NotImplementedError:
                continue

    def test_equality(self):
        for x, y in product(self.unique_transforms, self.unique_transforms):
            if x is y:
                self.assertTrue(x == y)
                self.assertFalse(x != y)
            else:
                self.assertFalse(x == y)
                self.assertTrue(x != y)

        self.assertTrue(IdentityTransform() == IdentityTransform().inv)
        self.assertFalse(IdentityTransform() != IdentityTransform().inv)

    def test_forward_inverse_cache(self):
        for transform in self.transforms:
            x = self._generate_data(transform).requires_grad_()
            try:
                y = transform(x)
            except NotImplementedError:
                continue
            x_2 = transform.inv(y)
            y_2 = transform(x_2)
            if transform.bijective:
                npt.assert_allclose(x_2.detach(), x.detach(), rtol=1e-4, atol=1e-4)
            else:
                npt.assert_allclose(y_2.detach(), y.detach(), rtol=1e-4, atol=1e-4)

    def test_forward_inverse_no_cache(self):
        for transform in self.transforms:
            x = self._generate_data(transform).requires_grad_()
            try:
                y = transform(x)
                x_2 = transform.inv(y.clone())  # bypass cache
                print(transform)
                print(x_2)
                # import pdb; pdb.set_trace()
                y_2 = transform(x_2)
            except NotImplementedError:
                continue

            if transform.bijective:
                npt.assert_allclose(x_2.detach(), x.detach(), rtol=1e-4, atol=1e-3)
            else:
                npt.assert_allclose(y_2.detach(), y.detach(), rtol=1e-4, atol=1e-4)

    def test_univariate_forward_jacobian(self):
        for transform in self.transforms:
            if transform.event_dim > 0:
                continue
            x = self._generate_data(transform).requires_grad_()
            try:
                y = transform(x)
                actual = transform.log_abs_det_jacobian(x, y)
            except NotImplementedError:
                continue
            expected = torch.abs(grad([y.sum()], [x])[0]).log()
            npt.assert_allclose(
                expected.detach(), actual.detach(), rtol=1e-6, atol=1e-6
            )

    def test_univariate_inverse_jacobian(self):
        for transform in self.transforms:
            if transform.event_dim > 0:
                continue
            y = self._generate_data(transform.inv).requires_grad_()
            try:
                x = transform.inv(y)
                actual = transform.log_abs_det_jacobian(x, y)
            except NotImplementedError:
                continue
            expected = -torch.abs(grad([x.sum()], [y])[0]).log()
            npt.assert_allclose(
                expected.detach(), actual.detach(), rtol=1e-6, atol=1e-6
            )

    def test_jacobian_shape(self):
        for transform in self.transforms:
            x = self._generate_data(transform)
            try:
                y = transform(x)
                actual = transform.log_abs_det_jacobian(x, y)
            except NotImplementedError:
                continue
            self.assertEqual(actual.shape, x.shape[: x.dim() - transform.event_dim])

    def test_transform_shapes(self):
        transform0 = ExpTransform()
        transform1 = SoftmaxTransform()
        transform2 = LowerCholeskyTransform()

        self.assertEqual(transform0.event_dim, 0)
        self.assertEqual(transform1.event_dim, 1)
        self.assertEqual(transform2.event_dim, 2)
        self.assertEqual(ComposeTransform([transform0, transform1]).event_dim, 1)
        self.assertEqual(ComposeTransform([transform0, transform2]).event_dim, 2)
        self.assertEqual(ComposeTransform([transform1, transform2]).event_dim, 2)

    def test_transformed_distribution_shapes(self):
        transform0 = ExpTransform()
        transform1 = SoftmaxTransform()
        transform2 = LowerCholeskyTransform()
        base_dist0 = dist.Normal(torch.zeros(4, 4), torch.ones(4, 4))
        base_dist1 = dist.Dirichlet(torch.ones(4, 4))
        base_dist2 = dist.Normal(torch.zeros(3, 4, 4), torch.ones(3, 4, 4))
        examples = [
            ((4, 4), (), base_dist0),
            ((4,), (4,), base_dist1),
            ((4, 4), (), dist.TransformedDistribution(base_dist0, [transform0])),
            ((4,), (4,), dist.TransformedDistribution(base_dist0, [transform1])),
            (
                (4,),
                (4,),
                dist.TransformedDistribution(base_dist0, [transform0, transform1]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist0, [transform0, transform2]),
            ),
            (
                (4,),
                (4,),
                dist.TransformedDistribution(base_dist0, [transform1, transform0]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist0, [transform1, transform2]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist0, [transform2, transform0]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist0, [transform2, transform1]),
            ),
            ((4,), (4,), dist.TransformedDistribution(base_dist1, [transform0])),
            ((4,), (4,), dist.TransformedDistribution(base_dist1, [transform1])),
            ((), (4, 4), dist.TransformedDistribution(base_dist1, [transform2])),
            (
                (4,),
                (4,),
                dist.TransformedDistribution(base_dist1, [transform0, transform1]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist1, [transform0, transform2]),
            ),
            (
                (4,),
                (4,),
                dist.TransformedDistribution(base_dist1, [transform1, transform0]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist1, [transform1, transform2]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist1, [transform2, transform0]),
            ),
            (
                (),
                (4, 4),
                dist.TransformedDistribution(base_dist1, [transform2, transform1]),
            ),
            ((3, 4, 4), (), base_dist2),
            ((3,), (4, 4), dist.TransformedDistribution(base_dist2, [transform2])),
            (
                (3,),
                (4, 4),
                dist.TransformedDistribution(base_dist2, [transform0, transform2]),
            ),
            (
                (3,),
                (4, 4),
                dist.TransformedDistribution(base_dist2, [transform1, transform2]),
            ),
            (
                (3,),
                (4, 4),
                dist.TransformedDistribution(base_dist2, [transform2, transform0]),
            ),
            (
                (3,),
                (4, 4),
                dist.TransformedDistribution(base_dist2, [transform2, transform1]),
            ),
        ]
        for batch_shape, event_shape, edist in examples:
            self.assertEqual(edist.batch_shape, batch_shape)
            self.assertEqual(edist.event_shape, event_shape)
            x = edist.rsample()
            try:
                edist.log_prob(x)  # this should not crash
            except NotImplementedError:
                continue


class TestFunctors(TestCase):
    def test_cat_transform(self):
        x_1 = -1 * torch.range(1, 100).view(-1, 100)
        x_2 = (torch.range(1, 100).view(-1, 100) - 1) / 100
        x_3 = torch.range(1, 100).view(-1, 100)
        t_1, t_2, t_3 = ExpTransform(), AffineTransform(1, 100), IdentityTransform()
        dim = 0
        x = torch.cat([x_1, x_2, x_3], dim=dim)
        transform = CatTransform([t_1, t_2, t_3], dim=dim)
        actual_dom_check = transform.domain.check(x)
        expected_dom_check = torch.cat(
            [t_1.domain.check(x_1), t_2.domain.check(x_2), t_3.domain.check(x_3)],
            dim=dim,
        )
        npt.assert_allclose(actual_dom_check, expected_dom_check)
        actual = transform(x)
        expected = torch.cat([t_1(x_1), t_2(x_2), t_3(x_3)], dim=dim)
        npt.assert_allclose(actual.detach(), expected)
        y_1 = torch.range(1, 100).view(-1, 100)
        y_2 = torch.range(1, 100).view(-1, 100)
        y_3 = torch.range(1, 100).view(-1, 100)
        y = torch.cat([y_1, y_2, y_3], dim=dim)
        actual_cod_check = transform.codomain.check(y)
        expected_cod_check = torch.cat(
            [t_1.codomain.check(y_1), t_2.codomain.check(y_2), t_3.codomain.check(y_3)],
            dim=dim,
        )
        npt.assert_allclose(actual_cod_check.detach(), expected_cod_check.detach())
        actual_inv = transform.inv(y)
        expected_inv = torch.cat([t_1.inv(y_1), t_2.inv(y_2), t_3.inv(y_3)], dim=dim)
        npt.assert_allclose(expected_inv.detach(), actual_inv.detach())
        actual_jac = transform.log_abs_det_jacobian(x, y)
        expected_jac = torch.cat(
            [
                t_1.log_abs_det_jacobian(x_1, y_1),
                t_2.log_abs_det_jacobian(x_2, y_2),
                t_3.log_abs_det_jacobian(x_3, y_3),
            ],
            dim=dim,
        )
        npt.assert_allclose(actual_jac.detach(), expected_jac.detach())
        self.assertIsInstance(transform.bijective, bool)

    @staticmethod
    def test_cat_transform_non_uniform():
        x_1 = -1 * torch.range(1, 100).view(-1, 100)
        x_2 = torch.cat(
            [
                (torch.range(1, 100).view(-1, 100) - 1) / 100,
                torch.range(1, 100).view(-1, 100),
            ]
        )
        t_1 = ExpTransform()
        t_2 = CatTransform([AffineTransform(1, 100), IdentityTransform()], dim=0)
        dim = 0
        x = torch.cat([x_1, x_2], dim=dim)
        transform = CatTransform([t_1, t_2], dim=dim, lengths=[1, 2])
        actual_dom_check = transform.domain.check(x)
        expected_dom_check = torch.cat(
            [t_1.domain.check(x_1), t_2.domain.check(x_2)], dim=dim
        )
        npt.assert_allclose(expected_dom_check, actual_dom_check)
        actual = transform(x)
        expected = torch.cat([t_1(x_1), t_2(x_2)], dim=dim)
        npt.assert_allclose(expected, actual)
        y_1 = torch.range(1, 100).view(-1, 100)
        y_2 = torch.cat(
            [torch.range(1, 100).view(-1, 100), torch.range(1, 100).view(-1, 100)]
        )
        y = torch.cat([y_1, y_2], dim=dim)
        actual_cod_check = transform.codomain.check(y)
        expected_cod_check = torch.cat(
            [t_1.codomain.check(y_1), t_2.codomain.check(y_2)], dim=dim
        )
        npt.assert_allclose(actual_cod_check, expected_cod_check)
        actual_inv = transform.inv(y)
        expected_inv = torch.cat([t_1.inv(y_1), t_2.inv(y_2)], dim=dim)
        npt.assert_allclose(expected_inv, actual_inv)
        actual_jac = transform.log_abs_det_jacobian(x, y)
        expected_jac = torch.cat(
            [t_1.log_abs_det_jacobian(x_1, y_1), t_2.log_abs_det_jacobian(x_2, y_2)],
            dim=dim,
        )
        npt.assert_allclose(actual_jac, expected_jac)

    def test_stack_transform(self):
        x_1 = -1 * torch.range(1, 100)
        x_2 = (torch.range(1, 100) - 1) / 100
        x_3 = torch.range(1, 100)
        t_1, t_2, t_3 = ExpTransform(), AffineTransform(1, 100), IdentityTransform()
        dim = 0
        x = torch.stack([x_1, x_2, x_3], dim=dim)
        transform = StackTransform([t_1, t_2, t_3], dim=dim)
        actual_dom_check = transform.domain.check(x)
        expected_dom_check = torch.stack(
            [t_1.domain.check(x_1), t_2.domain.check(x_2), t_3.domain.check(x_3)],
            dim=dim,
        )
        npt.assert_allclose(expected_dom_check, actual_dom_check)
        actual = transform(x)
        expected = torch.stack([t_1(x_1), t_2(x_2), t_3(x_3)], dim=dim)
        npt.assert_allclose(expected, actual)
        y_1 = torch.range(1, 100)
        y_2 = torch.range(1, 100)
        y_3 = torch.range(1, 100)
        y = torch.stack([y_1, y_2, y_3], dim=dim)
        actual_cod_check = transform.codomain.check(y)
        expected_cod_check = torch.stack(
            [t_1.codomain.check(y_1), t_2.codomain.check(y_2), t_3.codomain.check(y_3)],
            dim=dim,
        )
        npt.assert_allclose(actual_cod_check, expected_cod_check)
        actual_inv = transform.inv(x)
        expected_inv = torch.stack([t_1.inv(x_1), t_2.inv(x_2), t_3.inv(x_3)], dim=dim)
        npt.assert_allclose(expected_inv, actual_inv)
        actual_jac = transform.log_abs_det_jacobian(x, y)
        expected_jac = torch.stack(
            [
                t_1.log_abs_det_jacobian(x_1, y_1),
                t_2.log_abs_det_jacobian(x_2, y_2),
                t_3.log_abs_det_jacobian(x_3, y_3),
            ],
            dim=dim,
        )
        npt.assert_allclose(actual_jac, expected_jac)

        self.assertIsInstance(transform.bijective, bool)
