import unittest
from unittest import TestCase

import torch

from borch.tensor import RV, TransformedParameter
from borch import distributions
from borch.utils import random_variable_utils as rv_utils


class TestRandomVariableUtils(unittest.TestCase):
    def setUp(self):
        self.observed_mask = True, False, False, True
        self.random_vars = []
        for obs in self.observed_mask:
            # todo: breaks when dist arguments are python numbers..
            rv = RV(distributions.Normal(torch.ones(1), torch.ones(1)))
            if obs:
                rv.observe(torch.ones(1))
            self.random_vars.append(rv)

    def test_observed_random_variables_yields_all_observed(self):
        truth = [rv for rv, obs in zip(self.random_vars, self.observed_mask) if obs]
        result = list(rv_utils.observed_random_variables(self.random_vars))
        self.assertEqual(truth, result)

    def test_latent_random_variables_yields_all_latent(self):
        truth = [rv for rv, obs in zip(self.random_vars, self.observed_mask) if not obs]
        result = list(rv_utils.latent_random_variables(self.random_vars))
        self.assertEqual(truth, result)


class TestCopyRandomVariableProperties(unittest.TestCase):
    def setUp(self):
        self.mu = TransformedParameter(lambda x: x, torch.ones(1))
        self.sd = TransformedParameter(lambda x: x, torch.ones(1))
        normal = distributions.Normal(loc=self.mu, scale=self.sd)
        self.rv_old = RV(normal)
        self.rv_new = RV(distributions.Normal(1, 1))

    def test_observed_value_is_utilised(self):
        self.rv_new.observe(torch.tensor(100.0))
        rv_utils.copy_random_variable_properties(self.rv_old, self.rv_new)
        self.assertEqual(self.rv_old.observed, self.rv_new.observed)

    def test_transformed_parameters_are_copied(self):
        self.rv_new.observe(torch.tensor(100.0))
        rv_utils.copy_random_variable_properties(self.rv_old, self.rv_new)
        for name, arg in self.rv_new.distribution.arguments.items():
            self.assertIs(arg, self.rv_old.distribution.arguments[name])

    def test_copy_q_dist(self):
        rv_new = RV(distributions.Normal(loc=self.mu, scale=torch.ones(1)))
        rv_old = RV(
            distributions.Normal(loc=0, scale=torch.ones(1)),
            q_dist=distributions.Normal(loc=self.mu, scale=100 * torch.ones(1)),
        )
        rv_utils.copy_random_variable_properties(rv_old, rv_new)
        self.assertEqual(rv_new.arguments["scale"], 100)


class TestRequiresGrad(TestCase):
    def test_returns_false_for_number(self):
        self.assertFalse(rv_utils.requires_grad(1))

    def test_raises_type_error_if_not_tensor_or_numeric(self):
        with self.assertRaises(TypeError):
            rv_utils.requires_grad("a")

    def test_correctness_for_leaf_node_which_requires_grad(self):
        tensor = torch.randn(100, requires_grad=True)
        self.assertTrue(rv_utils.requires_grad(tensor))

    def test_correctness_for_leaf_node_which_doesnt_require_grad(self):
        tensor = torch.randn(100, requires_grad=False)
        self.assertFalse(rv_utils.requires_grad(tensor))

    def test_correctness_for_non_leaf_node_which_requires_grad(self):
        val = torch.randn(100, requires_grad=True)
        y_val = val * 10
        self.assertTrue(rv_utils.requires_grad(y_val))

    def test_correctness_for_non_leaf_node_which_doesnt_require_grad(self):
        val = torch.randn(100, requires_grad=False)
        y_val = val * 10
        self.assertFalse(rv_utils.requires_grad(y_val))

    def test_with_random_variable_which_does_not_require_grad(self):
        rv = RV(distributions.Normal(1, 1))
        self.assertFalse(rv_utils.requires_grad(rv))

    def test_with_random_variable_which_requires_grad_for_one_arg(self):
        mu = torch.tensor(1.0, requires_grad=True)
        rv = RV(distributions.Normal(mu, 1))
        self.assertTrue(rv_utils.requires_grad(rv))


class Test_named_observed_random_variables(TestCase):
    def setUp(self):
        self.rvs = {str(i): RV(distributions.Normal(0, 1)) for i in range(5)}

    def test_returns_correct_rv_no_observed(self):
        observed_rv = list(rv_utils.named_observed_random_variables(self.rvs.items()))
        self.assertEqual(len(observed_rv), 0)

    def test_returns_correct_rv_one_observed(self):
        self.rvs["1"].observe(torch.randn(1))
        observed_rv = list(rv_utils.named_observed_random_variables(self.rvs.items()))
        self.assertEqual(len(observed_rv), 1)
        self.assertEqual(id(observed_rv[0][1]), id(self.rvs["1"]))

    def test_returns_correct_rv_all_observed(self):
        for rv in self.rvs.values():
            rv.observe(torch.randn(1))
        observed_rv = list(rv_utils.named_observed_random_variables(self.rvs.items()))
        self.assertEqual(len(observed_rv), len(self.rvs))


if __name__ == "__main__":
    unittest.main()
