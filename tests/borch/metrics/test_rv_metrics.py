import unittest

import torch

import borch
from borch import RV, distributions
from borch.metrics import rv_metrics
from borch.utils.torch_utils import get_device

DEVICE = get_device()


class Test_metric(unittest.TestCase):
    def test_outputs_dict(self):
        rv = RV(
            distributions.Normal(
                torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE)
            )
        )
        rv.observe(torch.randn(2, 4).to(DEVICE))
        met = rv_metrics.all_metrics(rv)
        self.assertIsInstance(met, dict)

    def test_outputs_contains_mse_for_normal(self):
        rv = RV(
            distributions.Normal(
                torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE)
            )
        )
        rv.observe(torch.randn(2, 4).to(DEVICE))
        self.assertTrue(
            rv_metrics.mean_squared_error.__name__
            in borch.metrics.rv_metrics.all_metrics(rv)
        )

    def test_outputs_contains_accuracy_for_categorical(self):
        rv = RV(distributions.Categorical(logits=torch.randn(4, device=DEVICE)))
        rv.observe(torch.ones(1).long().to(DEVICE))
        self.assertTrue(
            rv_metrics.accuracy.__name__ in borch.metrics.rv_metrics.all_metrics(rv)
        )

    def test_no_supported_metric(self):
        rv = RV(distributions.Categorical(logits=torch.randn(4, device=DEVICE)))
        rv.observe(torch.ones(1).long().to(DEVICE))
        rv.support = torch.distributions.constraints.Constraint()
        self.assertFalse(borch.metrics.rv_metrics.all_metrics(rv))

    def test_non_observed_rv(self):
        with self.assertRaises(RuntimeError):
            rv = RV(distributions.Categorical(logits=torch.randn(4, device=DEVICE)))
            borch.metrics.rv_metrics.all_metrics(rv)


class Test_mean_squared_error(unittest.TestCase):
    metric = staticmethod(rv_metrics.mean_squared_error)

    def setUp(self):
        self.rv = RV(
            distributions.Normal(
                torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE)
            )
        )
        self.rv.observe(torch.randn(2, 4).to(DEVICE))

    def test_not_observed_raises_runtime_error(self):
        self.rv.observe(None)
        with self.assertRaises(RuntimeError):
            self.metric(self.rv)

    def test_correct_shape(self):
        mse = self.metric(self.rv)
        self.assertEqual(mse.shape, torch.Size([]))


class Test_accuracy(Test_mean_squared_error):
    metric = staticmethod(rv_metrics.accuracy)

    def setUp(self):
        self.rv = RV(distributions.Categorical(logits=torch.randn(4, device=DEVICE)))
        self.rv.observe(torch.ones(1).long().to(DEVICE))
