"""
Tensor primitives borch uses
"""
from borch.tensor.random_variable import RandomVariable, RV
from borch.tensor.transformed_parameter import TransformedParameter, TP
