from io import BytesIO
import pickle as pkl
from unittest import TestCase, mock

import torch

from borch import RV, TP, distributions, module, guide
from borch.utils.inference_conversion import pq_dict_to_lists
from borch.infer import negative_log_prob
from borch.optimizer_collection import OptimizersCollection

from tests.borch.test_module_base import TestModuleBase


class TestModule(TestModuleBase):
    _cls = module.Module
    # todo: also test for nested modules

    def setUp(self):
        super().setUp()
        self.module.guide = guide.AutomaticGuide()
        self.nested.guide = guide.AutomaticGuide()

    def test_proper_update_of_dist_arguments(self):
        self.module.observe(test=torch.zeros(1))
        self.module.test = RV(
            distributions.Normal(torch.ones(1, requires_grad=True), 1)
        )
        self.module.test = RV(
            distributions.Normal(torch.ones(10, requires_grad=True), 1)
        )
        self.assertEqual(tuple(self.module.test.distribution.sample().shape), (10,))

    def test_prior_getattrib(self):
        self.module.rv = self.rv
        self.assertIs(self.module.prior.rv, self.rv)

    def test_prior_getitem(self):
        self.module.rv = self.rv
        self.assertIs(self.module.prior["rv"], self.rv)

    def test_named_parameters_mix_tensor_transformed_param(self):
        mu = torch.tensor([1, 2, 3.0], requires_grad=True)
        sd = TP(lambda x: x.exp(), torch.nn.Parameter(torch.Tensor([1, 1, 1.0])))
        rv = RV(distributions.Normal(0, 1), q_dist=distributions.Normal(mu, sd))

        self.module.rv = rv
        observed = []
        for param in self.module.opt_parameters():
            observed.append(param)
        self.assertEqual(len(observed), 2)

    def test_guide_has_been_set(self):
        self.assertIsNotNone(self.module.guide)

    def test_automaticguide_used_as_default(self):
        # pylint: disable=unidiomatic-typecheck
        new_module = module.Module()
        self.assertTrue(type(new_module.guide) == guide.AutomaticGuide)

    def test_apply_also_applies_function_to_guide(self):
        func = mock.MagicMock()
        self.module.apply(func)
        for called_with in (
            self.module,
            self.module.guide,
            self.nested,
            self.nested.guide,
        ):
            func.assert_any_call(called_with)

    def test_apply_returns_self(self):
        func = mock.MagicMock()
        out = self.module.apply(func)
        self.assertEqual(id(out), id(self.module))

    def test_can_remove_guide_by_setting_it_to_none(self):
        self.module.guide = None
        self.assertNotIn("guide", self.module._modules)

    def test_raises_runtime_error_if_no_guide_is_set(self):
        self.module.guide = None
        with self.assertRaises(RuntimeError):
            self.module.test = self.rv

    def test_raises_value_error_if_guide_is_set_to_non_guide_object(self):
        with self.assertRaises(ValueError):
            self.module.guide = 1

    def test_module_does_not_return_random_variables_from_guide(self):
        # It would appear that this is desired behaviour
        self.module.guide.rv = self.rv
        rvs = tuple(self.module.random_variables())
        self.assertNotIn(id(self.rv), map(id, rvs))

    def test_can_pickle_using_torch_save_torch_load_on_module(self):
        pass  # todo: use mock and stringIO

    def test_adding_rv_to_module_adds_it_to_guide(self):
        name = "rv"
        setattr(self.module, name, self.rv)
        self.assertTrue(hasattr(self.module.guide, name))

    def test_sample_calls_rv_sample_for_discrete_distribution(self):
        # NB unlike the parent (ModuleBase) this should be called on the guide!
        self.module.rv = self.discrete_rv
        with mock.patch.object(self.module.guide.get_rv("rv"), "sample") as mocked:
            self.module.sample()
        mocked.assert_called()

    def test_sample_calls_rv_rsample_for_continuous_distribution(self):
        # NB unlike the parent (ModuleBase) this should be called on the guide!
        self.module.rv = self.rv
        with mock.patch.object(self.module.guide.get_rv("rv"), "rsample") as mocked:
            self.module.sample()
        mocked.assert_called()

    def test_getattr_calls_sample_on_discrete_rv_when_sampling(self):
        # NB unlike the parent (ModuleBase) this should be called on the guide!
        self.module.rv = self.discrete_rv
        self.module.sampling(True)
        with mock.patch.object(self.module.guide.get_rv("rv"), "sample") as mocked:
            _ = self.module.rv
        mocked.assert_called()

    def test_getattr_calls_rsample_on_continuous_rv_when_sampling(self):
        # NB unlike the parent (ModuleBase) this should be called on the guide!
        self.module.rv = self.rv
        self.module.sampling(True)
        with mock.patch.object(self.module.guide.get_rv("rv"), "rsample") as mocked:
            _ = self.module.rv
        mocked.assert_called()

    def test_getattr_observes_random_variable_when_observed_is_set(self):
        # NB unlike the parent (ModuleBase) this should be called on the guide!
        self.module.observe(test=torch.tensor([100.0]))
        self.module.test = self.rv
        with mock.patch.object(self.module.guide.get_rv("test"), "observe") as mocked:
            _ = self.module.test
        mocked.assert_called()

    def test_sample_calls_sample_on_all_rvs_in_guide(self):
        for rv in self.module.guide.random_variables():
            rv.rsample = mock.create_autospec(rv.rsample)
            rv.sample = mock.create_autospec(rv.sample)

        self.module.sample()

        for rv in self.module.guide.random_variables():
            self.assertTrue(rv.sample.called or rv.rsample.called)

    def test_named_random_variables_returns_rvs_from_guide(self):
        self.module.rv = self.rv  # add rv
        # NB torch doesnt play nice with some membership tests
        rv_ids = [id(rv) for _, rv in self.module.named_random_variables()]
        for rv in self.module.guide.random_variables():
            self.assertIn(id(rv), rv_ids)

    def test_named_random_variables_guide_returns_only_from_guide(self):
        self.module.rv = self.rv
        rv_ids = [id(rv) for _, rv in self.module.named_random_variables_guide()]
        guide_rv_ids = [id(rv) for _, rv in self.module.guide.named_random_variables()]

        self.assertEqual(len(rv_ids), len(guide_rv_ids))

        for rv_id in guide_rv_ids:
            self.assertIn(rv_id, rv_ids)

    def test_named_random_variables_guide_handles_nested_modules(self):
        self.module.rv = self.rv
        self.module.nested.rv2 = self.discrete_rv
        rv_ids = [id(rv) for _, rv in self.module.named_random_variables_guide()]
        guide_rv_ids = [id(rv) for _, rv in self.module.guide.named_random_variables()]
        guide_rv_ids += [
            id(rv) for _, rv in self.module.nested.guide.named_random_variables()
        ]

        self.assertEqual(len(rv_ids), len(guide_rv_ids))

        for rv_id in guide_rv_ids:
            self.assertIn(rv_id, rv_ids)

    def test_named_random_variables_prior_returns_only_from_prior(self):
        self.module.rv = self.rv
        rv_ids = [id(rv) for _, rv in self.module.named_random_variables_prior()]
        self.assertEqual(len(rv_ids), 1)
        self.assertEqual(rv_ids[0], id(self.rv))

    def test_named_random_variables_prior_handles_nested_modules(self):
        self.module.rv = self.rv
        self.module.nested.rv2 = self.discrete_rv
        rv_ids = [id(rv) for _, rv in self.module.named_random_variables_prior()]
        guide_rv_ids = [id(self.rv), id(self.discrete_rv)]

        self.assertEqual(len(rv_ids), len(guide_rv_ids))

        for rv_id in guide_rv_ids:
            self.assertIn(rv_id, rv_ids)

    def test_remove_random_variables_removes_rvs_from_self_and_children(self):
        # NB for Module this does not remove rvs from the guide!
        self.module.rv = self.rv  # add a random variable
        self.module.nested.rv2 = self.discrete_rv

        self.module.remove_random_variables()
        module_rvs = list(self.module._parameters.values())
        nested_rvs = list(self.module.nested._parameters.values())
        self.assertFalse(bool(module_rvs + nested_rvs))

    def test_remove_random_variables_does_not_rvs_from_guide(self):
        self.module.rv = self.rv  # add a random variable
        self.module.remove_random_variables()
        rvs_on_guide = tuple(self.module.guide.random_variables())
        self.assertTrue(bool(rvs_on_guide))

    def test_pq_dict_values_are_rvs_from_guide(self):
        self.module.rv = self.rv  # add a random variable
        pq_dict = self.module.pq_dict()
        self.assertTrue(torch.equal(self.module.rv, list(pq_dict.values())[0]))

    def test_pq_dict_keys_are_rvs_from_module(self):
        self.module.rv = self.rv
        pq_dict = self.module.pq_dict()
        self.assertTrue(torch.equal(self.rv, list(pq_dict.keys())[0]))

    def test_pq_dict_is_empty_after_sample(self):
        self.module.rv = self.rv
        self.module.sample()
        self.assertFalse(self.module.pq_dict())

    def test_pq_dict_returns_only_used_rvs_after_sample_setattr(self):
        self.module.rv = self.rv
        self.assertTrue(self.module.pq_dict())

    def test_pq_dict_returns_only_used_rvs_after_sample_getattr(self):
        self.module.rv = self.rv
        self.module.sample()
        _ = self.module.rv
        self.assertTrue(self.module.pq_dict())

    def test_pq_to_infer(self):
        self.module.rv = self.rv
        self.assertIsInstance(self.module.pq_to_infer(), dict)

    def test_pq_works_for_intermediate_torch_module(self):
        intermediate = torch.nn.Linear(2, 2)
        nested = self._cls()
        nested.rv = self.rv
        intermediate.add_module("nested", nested)
        self.module.add_module("intermediate", intermediate)

        pq_dict = self.module.pq_dict()
        self.assertIn(self.rv, pq_dict.keys())

    def test_observe_rv(self):
        # legacy test
        self.module.guide = guide.AutomaticGuide()
        self.module.observe(rv1=torch.ones(1).sum())
        optimizer = OptimizersCollection(optimizer=torch.optim.Adam, lr=0.01)
        for _ in range(3):
            self.module.remove_random_variables()
            self.module.sample()
            optimizer.zero_grad()

            # add random variables to module (approximating distributions will
            # be automatically added depending on the assigned guide)
            self.module.rv1 = RV(distributions.Normal(1, 1))
            self.module.rv2 = RV(distributions.Normal(1, 1))

            # construct a loss
            kwargs = pq_dict_to_lists(self.module.pq_dict())
            loss = negative_log_prob(**kwargs)

            loss.backward()
            optimizer.step(self.module.opt_parameters())

        self.assertEqual(self.module.rv1, 1)

    def test_observe_rv_point_mass_guide(self):
        # legacy test
        self.module.guide = guide.PointMassGuide()
        self.module.observe(rv1=torch.ones(1).sum())
        optimizer = OptimizersCollection(optimizer=torch.optim.Adam, lr=0.01)
        for _ in range(3):
            self.module.remove_random_variables()
            # self.module.sample()  # cannot sample before rvs have been added
            optimizer.zero_grad()

            # add random variables to module (approximating distributions will
            # be automatically added depending on the assigned guide)
            self.module.rv1 = RV(distributions.Normal(1, 1))
            self.module.rv2 = RV(distributions.Normal(1, 1))

            # sampling here causes non-gradient tensors to be returned.. what
            # should we do on the PointMass distribution? (see todo in there)

            # construct a loss
            kwargs = pq_dict_to_lists(self.module.pq_dict())
            loss = negative_log_prob(**kwargs)

            loss.backward()
            optimizer.step(self.module.opt_parameters())

        self.assertEqual(self.module.rv1, 1)

    def test_half(self):
        self.module.rv = self.rv
        self.module.half()
        self.assertEqual(self.module.rv.dtype, torch.float16)


class Inheritor(module.Module):
    def __init__(self):
        super().__init__(guide=guide.AutomaticGuide())

        mu = torch.tensor([1, 2, 3.0], requires_grad=True)
        sd = torch.tensor([1, 1, 1.0], requires_grad=True)

        self.rv1 = RV(distributions.Cauchy(mu, sd))

    def forward(self, *inputs):
        pass


def no_op(x):  # pylint: disable=missing-docstring
    return x


class TestModuleSerialisation(TestCase):
    def setUp(self):
        mu = TP(no_op, torch.tensor([1, 2, 3.0], requires_grad=True))
        sd = TP(no_op, torch.tensor([1, 1, 1.0], requires_grad=True))
        rv = RV(distributions.Normal(mu, sd))

        f = BytesIO()
        self.old = Inheritor()
        self.old.rv2 = rv
        pkl.dump(self.old.state_dict(), f)  # .rv1, f)
        f.seek(0)

        self.new = Inheritor()
        self.state_dict = pkl.load(f)
        self.new.load_state_dict(self.state_dict)

    def assert_rvs_equal(self, rv_old, rv_new):  # pylint: disable=invalid-name
        self.assertTrue(torch.equal(rv_old, rv_new))
        self.assertEqual(type(rv_old.distribution), type(rv_new.distribution))
        for name in rv_old.arguments:
            self.assertTrue(torch.equal(rv_old.arguments[name], rv_new.arguments[name]))
            self.assertIsInstance(rv_old.arguments[name], type(rv_new.arguments[name]))

    def test_module_rvs_are_retained(self):
        for name, rv in self.old._parameters.items():
            self.assert_rvs_equal(self.new._parameters[name], rv)

    def test_guide_rvs_are_retained(self):
        for name, rv in self.old.guide._parameters.items():
            self.assert_rvs_equal(self.new.guide._parameters[name], rv)

    def test_original_state_dict_retains_all_elements_after_loading(self):
        orig = self.old.state_dict()
        union = set(orig.keys()).intersection(self.state_dict.keys())
        self.assertTrue(len(union) == len(orig))


if __name__ == "__main__":
    import unittest

    unittest.main()
