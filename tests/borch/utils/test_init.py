import unittest

from borch.utils import init


class Test_xavier_normal_std(unittest.TestCase):
    def test_one_dim_input(self):
        sd = init.xavier_normal_std((100,))
        self.assertAlmostEqual(sd, 0.14071950894605836)

    def test_three_dim_input(self):
        sd = init.xavier_normal_std((100, 100, 100))
        self.assertEqual(sd, 0.01)


class Test_kaiming_normal_std(unittest.TestCase):
    def test_one_dim_input(self):
        sd = init.kaiming_normal_std((100,))
        self.assertAlmostEqual(sd, 0.1)

    def test_three_dim_input(self):
        sd = init.kaiming_normal_std((100, 100, 100))
        self.assertAlmostEqual(sd, 0.01)

    def test_one_dim_input_leaky_relu(self):
        sd = init.kaiming_normal_std((100,), nonlinearity="leaky_relu")
        self.assertAlmostEqual(sd, 0.05773502691896257)

    def test_three_dim_input_leaky_relu(self):
        sd = init.kaiming_normal_std((100, 100, 100), nonlinearity="leaky_relu")
        self.assertAlmostEqual(sd, 0.005773502691896257)


class Test__calculate_fan_in_and_fan_out(unittest.TestCase):
    def test_one_dim_input(self):
        with self.assertRaises(ValueError):
            init._calculate_fan_in_and_fan_out((10,))

    def test_two_dim_input(self):
        fan_in, fan_out = init._calculate_fan_in_and_fan_out((10, 100))
        self.assertEqual(fan_in, 100)
        self.assertEqual(fan_out, 10)

    def test_three_dim_input(self):
        fan_in, fan_out = init._calculate_fan_in_and_fan_out((10, 10, 5))
        self.assertEqual(fan_in, 50)
        self.assertEqual(fan_out, 50)

    def test_four_dim_input(self):
        fan_in, fan_out = init._calculate_fan_in_and_fan_out((10, 10, 5, 4))
        self.assertEqual(fan_in, 200)
        self.assertEqual(fan_out, 200)


class Test__calculate_correct_fan(unittest.TestCase):
    def test_one_dim_input(self):
        with self.assertRaises(ValueError):
            init._calculate_correct_fan((10,), "fan_in")

    def test_two_dim_input(self):
        fan = init._calculate_correct_fan((10, 100), "fan_in")
        self.assertEqual(fan, 100)

    def test_three_dim_input(self):
        fan = init._calculate_correct_fan((10, 10, 5), "fan_in")
        self.assertEqual(fan, 50)

    def test_non_valid_mode(self):
        with self.assertRaises(ValueError):
            init._calculate_correct_fan((10, 100), "fan")


class Test_calculate_gain(unittest.TestCase):
    def test_calculate_gain_linear(self):
        for fnc in [
            "linear",
            "conv1d",
            "conv2d",
            "conv3d",
            "conv_transpose2d",
            "conv_transpose2d",
            "conv_transpose3d",
        ]:
            gain = init.calculate_gain(fnc)
            self.assertEqual(gain, 1)

    def test_calculate_gain_nonlinear(self):
        for fnc in ["sigmoid", "tanh", "relu", "leaky_relu"]:
            gain = init.calculate_gain(fnc)
            if fnc == "sigmoid":
                self.assertEqual(gain, 1)
            elif fnc == "tanh":  # 5 / 3
                self.assertEqual(gain, 1.6666666666666667)
            elif fnc == "relu":  # sqrt(2)
                self.assertEqual(gain, 1.4142135623730951)
            elif fnc == "leaky_relu":  # sqrt(2 / 1 + slope^2))
                self.assertEqual(gain, 1.4141428569978354)

    def test_calculate_gain_leaky_relu(self):
        for param in [None, 0, 0.01, 10]:
            gain = init.calculate_gain("leaky_relu", param)
            if param is None:  # Default slope is 0.01
                self.assertEqual(gain, 1.4141428569978354)
            elif param == 0:  # No slope = same gain as normal ReLU
                self.assertEqual(gain, 1.4142135623730951)
            elif param == 0.01:
                self.assertEqual(gain, 1.4141428569978354)
            elif param == 10:
                self.assertEqual(gain, 0.14071950894605836)

    def test_calculate_gain_leaky_relu_only_accepts_numbers(self):
        for param in [True, [1], {"a": "b"}]:
            with self.assertRaises(ValueError):
                init.calculate_gain("leaky_relu", param)

    def test_calculate_gain_only_accepts_valid_nonlinearities(self):
        with self.assertRaises(ValueError):
            init.calculate_gain("some_random_string_1")


if __name__ == "__main__":
    unittest.main()
