from io import BytesIO
from itertools import chain
import pickle as pkl
from unittest import TestCase, mock

import numpy as np
import pytest
import torch

from borch.tensor import RandomVariable, TransformedParameter
from borch.distributions import distributions


def no_op(x):  # pylint: disable=missing-docstring
    return x


class TestRandomVariable(TestCase):
    def setUp(self):
        # arguments
        self.mu_real = torch.ones(3, requires_grad=True)
        self.sd_u = torch.zeros(3, requires_grad=True)
        self.sd = TransformedParameter(torch.exp, self.sd_u)

        # todo: fix this workaround
        # required workaround because distributions use `broadcast_all` which
        # means that the `dist.loc` tensor is no longer a leaf node...
        self.mu = TransformedParameter(no_op, self.mu_real)

        # distributions
        self.dist = distributions.Normal(self.mu, self.sd)
        self.q_dist = distributions.Normal(torch.ones(3), torch.ones(3))

        # random variable
        self.rvar = RandomVariable(self.dist, self.q_dist)

    def test_repr(self):
        prefix = "RandomVariable containing:\n"
        self.assertTrue(repr(self.rvar).startswith(prefix))

    def test_requires_grad_at_init(self):
        if self.rvar.has_rsample:
            self.assertTrue(self.rvar.requires_grad)
        else:
            self.assertFalse(self.rvar.requires_grad)

    def test__recreate_distribution_creates_new_object(self):
        original_id = id(self.rvar.distribution)
        self.rvar.recalculate()
        self.assertNotEqual(original_id, id(self.rvar.distribution))

    def test_doesnt_require_grad_at_init_if_inputs_did_not(self):
        dist = distributions.Normal(self.mu.detach(), self.sd.detach())
        rvar = RandomVariable(dist)
        self.assertFalse(rvar.requires_grad)

    def test_parameters_returns_the_unconstrained_sd(self):
        self.assertIn(id(self.sd_u), map(id, self.rvar.parameters()))

    def test_parameters_returns_mu(self):
        self.assertIn(id(self.mu_real), map(id, self.rvar.parameters()))

    def test_parameters_returns_all_parameters(self):
        """There should be two parameters returned: mu and unconstrained sd."""
        self.assertEqual(len(tuple(self.rvar.parameters())), 2)

    def test_rsample_returns_new_values(self):
        self.assertFalse(torch.equal(self.rvar.data, self.rvar.rsample()))

    def test_sample_returns_new_values(self):
        old_data = self.rvar.data
        new_sample = self.rvar.sample()
        self.assertFalse(torch.equal(old_data, new_sample))

    def test_rsample_returns_a_new_object(self):
        self.assertIsNot(self.rvar.rsample(), self.rvar)

    def test_rsample_values_are_retained_in_rvar_data(self):
        sample = self.rvar.rsample()
        self.assertTrue(torch.equal(sample, self.rvar.data))

    def test_sample_returns_a_new_object(self):
        self.assertIsNot(self.rvar.sample(), self.rvar)

    def test_sample_values_are_retained_in_rvar_data(self):
        sample = self.rvar.sample()
        self.assertTrue(torch.equal(sample, self.rvar.data))

    def test_unobserving_calls_sample_when_previously_observed(self):
        self.rvar = RandomVariable(distributions.Poisson(3))
        self.rvar.observe(torch.tensor(1.0))
        with mock.patch.object(self.rvar, "_do_sample") as mocked:
            self.rvar.observe(None)
        mocked.assert_called()

    def test_unobserving_calls_rsample_when_previously_observed(self):
        self.rvar.observe(self.mu_real)
        with mock.patch.object(self.rvar, "_do_sample") as mocked:
            self.rvar.observe(None)
        mocked.assert_called()

    def test_setting_observe_to_none_doesnt_draw_a_needless_rsample(self):
        # if the rv was previously unobserved then setting observe(None)
        # should do nothing
        with mock.patch.object(self.rvar, "_do_sample") as mocked:
            self.rvar.observe(None)
        mocked.assert_not_called()

    def test_self_and_all_attributes_are_cast_to_double_when_applied(self):
        self.rvar.double()
        for param in chain(
            self.rvar, self.rvar.loc, self.rvar.scale, self.rvar.parameters()
        ):
            self.assertIs(param.dtype, torch.float64)

    def test_gradients_propagate_to_input_variables(self):
        (self.rvar.rsample() * 3).sum().backward()
        self.assertIsNotNone(self.mu_real.grad)
        self.assertIsNotNone(self.sd_u.grad)

    def test__do_sample_wrong_input(self):
        self.assertRaises(ValueError, lambda: self.rvar._do_sample("wrong_method", 1))

    def test_set_value_with_number(self):
        self.assertTrue(torch.equal(self.rvar.set_value_(1), torch.ones(3)))

    def test_observe(self):
        mu = torch.ones(1, requires_grad=True)
        rvar = RandomVariable(
            distributions.Normal(
                loc=TransformedParameter(torch.exp, torch.ones(1)), scale=1
            )
        ).observe(mu)
        self.assertEqual(mu, rvar)
        self.assertEqual(mu, rvar.sample())
        self.assertEqual(mu, rvar.rsample())

        new_obs = 2 * mu
        rvar.observe(new_obs)
        self.assertEqual(new_obs, rvar)
        self.assertEqual(new_obs, rvar.sample())
        self.assertEqual(new_obs, rvar.rsample())

    def test_observe_returns_self(self):
        self.assertEqual(id(self.rvar), id(self.rvar.observe(torch.ones(1))))

    def test_gradient_of_mu_is_1_after_only_rsample(self):
        self.rvar.rsample()
        self.rvar.sum().backward()
        self.assertTrue(torch.equal(self.mu_real.grad, torch.ones_like(self.mu_real)))

    def test_multiple_backward_passes_using_log_prob(self):
        for _ in range(3):
            self.rvar.rsample()
            loss = -(self.rvar.log_prob() * 3).sum()
            loss.backward()
            self.assertIsNotNone(self.mu_real.grad)

    def test_multiple_backward_passes_using_matmul(self):
        data = torch.ones(10, 3, requires_grad=False)
        for _ in range(3):
            self.rvar.rsample()
            loss = data.matmul(self.rvar).sum()
            loss.backward()
            self.assertIsNotNone(self.mu_real.grad)

    def test_backward_after_multiple_samples_using_log_prob(self):
        loss = 0
        for _ in range(3):
            self.rvar.rsample()
            loss += self.rvar.log_prob().sum()
        loss.backward()
        self.assertIsNotNone(self.mu_real.grad)

    def test_backward_multiple_samples_no_transformed_parameters(self):
        mu, sigma = torch.ones(1, requires_grad=True), torch.ones(1, requires_grad=True)
        rvar = RandomVariable(distributions.Normal(mu, sigma))
        loss = 0
        for _ in range(3):
            rvar.rsample()
            loss += rvar.log_prob().sum() + torch.exp(rvar)
        loss.backward()
        self.assertIsNotNone(mu.grad)

    def test_backward_after_multiple_samples_using_matmul(self):
        """NB in order for this to work the object returned from `.rsample`
        must be used! It is in fact a new node in the graph which must be used
        in order to avoid issues with backpropagating through inplace
        operations."""
        data = torch.ones(10, 3, requires_grad=False)
        loss = 0
        for _ in range(3):
            loss += data.matmul(self.rvar.rsample()).sum()
        loss.backward()
        self.assertIsNotNone(self.mu_real.grad)

    def test_double_survives_sample(self):
        self.rvar.double()
        self.assertEqual(self.rvar.dtype, torch.float64)
        self.rvar.sample()
        self.assertEqual(self.rvar.dtype, torch.float64)

    def test_is_still_a_random_variable_when_unpickled(self):
        f = BytesIO()
        pkl.dump(self.rvar, f)
        f.seek(0)
        pickled_rvar = pkl.load(f)

        self.assertIsInstance(pickled_rvar, RandomVariable)

    def test_is_still_a_random_variable_when_using_torch_load(self):
        f = BytesIO()
        torch.save(self.rvar, f)
        f.seek(0)
        pickled_rvar = torch.load(f)

        self.assertIsInstance(pickled_rvar, RandomVariable)

    def test_parameters_are_same_after_pickling(self):
        f = BytesIO()
        pkl.dump(self.rvar, f)
        f.seek(0)
        pickled_rvar = pkl.load(f)

        for pickled_parameter, parameter in zip(
            pickled_rvar.parameters(), self.rvar.parameters()
        ):
            np.testing.assert_equal(
                pickled_parameter.detach().numpy(), parameter.detach().numpy()
            )

    def test_has_same_distribution_after_pickling(self):
        f = BytesIO()
        pkl.dump(self.rvar, f)
        f.seek(0)
        pickled_rvar = pkl.load(f)

        dist = self.rvar.distribution
        pickled_dist = pickled_rvar.distribution

        np.testing.assert_equal(
            dist.loc.detach().numpy(), pickled_dist.loc.detach().numpy()
        )
        np.testing.assert_equal(
            dist.scale.detach().numpy(), pickled_dist.scale.detach().numpy()
        )
        self.assertIsInstance(pickled_rvar.distribution, distributions.Normal)

    def test_has_same_value_after_pickling(self):
        f = BytesIO()
        pkl.dump(self.rvar, f)
        f.seek(0)
        pickled_rvar = pkl.load(f)

        np.testing.assert_equal(
            self.rvar.detach().numpy(), pickled_rvar.detach().numpy()
        )

    def test__apply_recurse(self):
        self.rvar.to(torch.float64)
        for par in self.rvar.distribution.parameters():
            self.assertEqual(par.dtype, torch.float64)

        self.rvar.to(torch.float32)
        for par in self.rvar.distribution.parameters():
            self.assertEqual(par.dtype, torch.float32)

    def test_named_parameters_recurse_properly(self):
        sd = torch.ones(3)
        dist = distributions.Normal(self.rvar, sd)
        rvar = RandomVariable(dist)

        seen = set()
        for _, _ in rvar.named_parameters(seen=seen):
            pass

        self.assertIn(self.rvar, seen)
        self.assertIn(sd, seen)
        for par in self.rvar.parameters():
            self.assertIn(par, seen)

    @pytest.mark.skipif(not torch.cuda.is_available(), reason="no gpu")
    def test_random_variable_sent_to_device(self):
        self.rvar.to("cuda")
        self.assertEqual(self.mu_real.device.type, "cuda")
        self.assertEqual(self.sd_u.device.type, "cuda")

    @pytest.mark.skipif(not torch.cuda.is_available(), reason="no gpu")
    def test_random_variable_sent_to_device_hiearchically(self):
        sd = TransformedParameter(torch.exp, torch.zeros(3))
        rvar = RandomVariable(distributions.Normal(self.rvar, sd))
        rvar.to("cuda")
        for par in rvar.parameters():
            self.assertEqual(par.device.type, "cuda")

    def test_to_device_returns_self(self):
        new = self.rvar.to("cpu")
        self.assertIs(new, self.rvar)
