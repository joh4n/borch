from unittest import TestCase

import torch
from torch import nn

from borch.nn import Recall


class TestRecall(TestCase):
    # pylint: disable=unnecessary-lambda,arguments-differ

    def test_simple(self):
        linear = nn.Linear(3, 4)
        net = nn.Sequential(
            nn.Sequential(nn.Linear(2, 3), linear),
            nn.Sequential(nn.Linear(4, 5), Recall(linear, agg_fn=lambda x: x)),
        )
        output = net(torch.ones(10, 2))
        self.assertEqual(tuple(output[0].shape), (10, 5))
        self.assertEqual(tuple(output[1].shape), (10, 4))

    def test_complex(self):
        class Net(nn.Module):
            def __init__(self):
                super().__init__()
                self.linears = nn.ModuleList(
                    [nn.Linear(2, 4), nn.Linear(4, 4), nn.Linear(4, 4)]
                )
                self.recall = Recall(*self.linears, agg_fn=lambda x: sum(x))

            def forward(self, x):
                for mod in self.linears:
                    x = mod(x)
                return self.recall()  # NB we do not feed any data

        output = Net()(torch.ones(10, 2))
        self.assertEqual(tuple(output.shape), (10, 4))
