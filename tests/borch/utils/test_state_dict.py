from unittest import TestCase, skipUnless

import torch

from borch import nn
from borch import RV, distributions
from borch.utils.state_dict import (
    saveable_state_dict,
    copy_state_dict,
    unobserve_state_dict_,
    state_dict_to_device_,
)
from borch.utils.torch_utils import get_device


class Test_saveable_state_dict(TestCase):
    def setUp(self):
        self.net = Net()
        x = torch.randn(3)
        self.net(x)

    def tearDown(self):
        del self.net

    def test_state_dict_is_dict(self):
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        self.assertIsInstance(new_state_dict, dict)

    def test_state_dict_is_unobserved_when_not_observing_on_net(self):
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        for key in state_dict:
            self.assertIsNone(new_state_dict[key].observed)

    def test_state_dict_is_unobserved_when_observing_on_net(self):
        self.net.observe(classification=torch.tensor(0))
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        for key in state_dict:
            self.assertIsNone(new_state_dict[key].observed)

    def test_state_dict_is_unobserved_when_observing_on_gpu(self):
        if torch.cuda.is_available():
            self.net.to("cuda")
            self.net.observe(classification=torch.tensor(0))
            state_dict = self.net.state_dict()
            new_state_dict = saveable_state_dict(state_dict)
            for key in state_dict:
                self.assertIsNone(new_state_dict[key].observed)

    @skipUnless(torch.cuda.is_available(), "requires GPU")
    def test_state_dict_is_cpu_when_observing_on_gpu(self):
        self.net.to("cuda")
        self.net.observe(classification=torch.tensor(0, device="cuda"))
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        for tensor in new_state_dict.values():
            self.assertEqual(tensor.device.type, "cpu")

    def test_net_is_unobserved_after_loading(self):
        net2 = Net()
        self.net.observe(classification=torch.tensor(0))
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        net2.load_state_dict(new_state_dict)
        self.assertIsNone(net2.classification.observed)

    def test_net_is_observed_after_saveable_state_dict(self):
        self.net.observe(classification=torch.tensor(0))
        state_dict = self.net.state_dict()
        saveable_state_dict(state_dict)
        self.assertIsNotNone(self.net.classification.observed)


class Test_unobserve_state_dict(TestCase):
    def setUp(self):
        net = Net()
        net(torch.randn(3))
        self.state_dict = net.state_dict()

    def tearDown(self):
        del self.state_dict

    def test_unobserves_variables(self):
        unobserved_state_dict = unobserve_state_dict_(self.state_dict)
        for val in unobserved_state_dict.values():
            if hasattr(val, "observed"):
                self.assertIsNone(val.observed)

    def test_that_state_dict_becomes_unobserved(self):
        net = Net()
        net(torch.randn(3))
        state_dict = net.state_dict()
        state_dict["classification"].observe(torch.ones(1))

        self.assertIsNotNone(state_dict["classification"].observed)
        unobserve_state_dict_(state_dict)
        self.assertIsNone(state_dict["classification"].observed)


class Test_state_dict_to_device(TestCase):
    @staticmethod
    def test_sending_state_dict_to_float16():
        net = Net()
        state_dict = state_dict_to_device_(net.state_dict(), torch.float64)
        for tensor in state_dict.values():
            assert tensor.dtype == torch.float64

    def test_all_params_are_on_cpu(self):
        net = Net()
        net(torch.randn(3))
        net.to(get_device())

        state_dict = state_dict_to_device_(net.state_dict(), "cpu")
        for val in state_dict.values():
            if hasattr(val, "parameters"):
                for par in val.parameters():
                    self.assertEqual(par.device.type, "cpu")
            self.assertEqual(val.device.type, "cpu")


class Test_copy_state_dict(TestCase):
    def setUp(self):
        net = Net()
        net(torch.randn(3))
        self.state_dict = net.state_dict()

    def tearDown(self):
        del self.state_dict

    def test_original_state_dict_stays_same(self):
        before_ids = get_state_dict_ids(self.state_dict)
        copy_state_dict(self.state_dict)
        after_ids = get_state_dict_ids(self.state_dict)
        for before_id, after_id in zip(before_ids, after_ids):
            self.assertEqual(before_id, after_id)

    def test_new_state_dict_is_new(self):
        original_ids = get_state_dict_ids(self.state_dict)
        copied_ids = get_state_dict_ids(copy_state_dict(self.state_dict))
        for original_id, copied_id in zip(original_ids, copied_ids):
            self.assertNotEqual(original_id, copied_id)

    def test_doesnt_unobserve_original(self):
        self.state_dict["classification"].observe(torch.ones(1))
        copy = copy_state_dict(self.state_dict)
        unobserve_state_dict_(copy)
        self.assertIsNotNone(self.state_dict["classification"].observed)

    def test_original_state_dict_is_untouched_after_assigning_data(self):
        state_dict = {"rv": RV(distributions.Normal(0, 0))}
        new_state_dict = copy_state_dict(state_dict)
        new_state_dict["rv"].data = torch.Tensor(1000)
        self.assertNotEqual(state_dict["rv"].item(), 1000)


def get_state_dict_ids(state_dict):
    ids = []
    for val in state_dict.values():
        if hasattr(val, "parameters"):
            for par in val.parameters():
                ids.append(id(par))
            ids.append(id(val))
    return ids


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Linear(3, 3)

    # pylint: disable=arguments-differ, attribute-defined-outside-init
    def forward(self, x):
        x = self.layer_1(x)
        self.classification = RV(distributions.Categorical(logits=x))
        return self.classification
