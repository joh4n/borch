"""
Functions to detect git info
"""
import subprocess


def capture_terminal_cmd(command):
    """ Captures the output from a command run in terminal.

    Args:
        command (str): the command to run

    Returns:
        string, the output of the command
    """
    output = subprocess.run(
        command, shell=True, stdout=subprocess.PIPE, universal_newlines=True
    )
    return output.stdout


def release_notes_from_git():  # pragma: no cover
    """Generate release notes from git, it will look trough the git log
    and return all merge reqest comments since the last tag.

    Returns:
        string, the generated release notes
    """
    log = capture_terminal_cmd("git log $(git describe --tags --abbrev=0)..HEAD")
    out = []
    git_entries = log.split("commit ")
    for ent in git_entries:
        if "Merge branch " in ent:
            mrg_commit = ent.split("Merge branch ")[1]
            for mr_line in mrg_commit.split("\n"):
                if "into 'master'" not in mr_line:
                    out.append(mr_line)
    return "\n".join(out)


if __name__ == "__main__":
    print(release_notes_from_git())
