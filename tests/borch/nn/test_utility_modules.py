from unittest import TestCase

import torch

from borch import nn
from borch.nn.utility_modules import Identity, Apply, Cat


class Test_Identity(TestCase):
    def setUp(self):
        self.identity = Identity()
        self.x = torch.randn(3, 3)

    def test_tensor(self):
        self.assertTrue(torch.all(torch.eq(self.x, self.identity(self.x))))

    def test_tuple(self):
        tup = (self.x, self.x)
        self.assertTrue(torch.all(torch.eq(self.x, self.identity(tup)[0])))

    def test_tuple_type(self):
        tup = (self.x, self.x)
        self.assertIsInstance(self.identity(tup), tuple)


class Test_Apply(TestCase):
    def test_sum(self):
        summer = Apply(lambda x: x.sum())
        x = torch.ones(4)
        self.assertEqual(summer(x).item(), x.sum().item())

    def test_add_tuple_elements(self):
        adder = Apply(lambda x: x[0] + x[1])
        x = torch.randn(4, 4)
        y = torch.randn(4, 4)
        self.assertTrue(torch.all(torch.eq(x + y, adder((x, y)))))

    def test_can_forward_variadic_input(self):
        concatter = Apply(lambda *x: torch.cat(x, 1))
        x = torch.randn(1, 3, 16, 16)
        y = torch.randn(1, 6, 16, 16)
        self.assertEqual(concatter(x, y).shape, torch.Size([1, 9, 16, 16]))

    def test_skip(self):
        skip = nn.Sequential(
            nn.Concurrent(nn.Conv2d(3, 8, 3, padding=1), nn.Identity()),
            nn.Apply(lambda x, y: torch.cat((x, y), 1)),
        )
        x = torch.randn(1, 3, 16, 16)
        self.assertEqual(skip(x).shape, torch.Size([1, 11, 16, 16]))


class Test_Cat(TestCase):
    def test_concatenate(self):
        featuremap1 = torch.randn(2, 3, 16, 16)
        featuremap2 = torch.randn(2, 4, 16, 16)
        featuremap3 = torch.randn(2, 5, 16, 16)

        concatter = Cat()

        featuremap = concatter(featuremap1, featuremap2, featuremap3)

        self.assertEqual(featuremap.shape, torch.Size([2, 12, 16, 16]))
