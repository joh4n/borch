"""
Distributions to use with borch.

It basically does some minor modifications to torch.distributions
"""
from borch.distributions.distributions import *
from borch.distributions import constraints, constraints_registry, transforms
