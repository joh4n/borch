"""
Tests for the borch.ppl.distributions file
"""
from numbers import Number
import io
import pickle
from types import GeneratorType
from unittest import TestCase

import numpy as np
import torch
from torch import distributions as dist
from borch import TP
from borch.distributions import (
    distributions,
    constraints,
    transforms,
    distribution_utils,
)


class TestDistribution(TestCase):
    _distribution = distributions.Distribution
    _init_args = {}

    def setUp(self):
        self.dist = self._distribution(**self._init_args)

    def test__parameters_is_dict(self):
        self.assertIsInstance(self.dist._parameters, dict)

    def test_named_parameters_is_generator(self):
        self.assertIsInstance(self.dist.named_parameters(), GeneratorType)

    def test_parameters_is_generator(self):
        self.assertIsInstance(self.dist.parameters(), GeneratorType)

    def test_recreated_distribution_inherits_from_borch_distribution(self):
        self.assertIsInstance(self.dist, distributions.Distribution)

    def test_object_arguments_equal_init_args(self):
        self.assertEqual(self.dist.arguments, self._init_args)

    def test_recalculate_returns_a_new_object(self):
        new = self.dist.recalculate()
        self.assertIsNot(self.dist, new)

    def test_recalculate_returns_an_object_of_the_same_type(self):
        new = self.dist.recalculate()
        self.assertEqual(type(self.dist), type(new))

    def test_recalculate_arguments_equal_init_args(self):
        new = self.dist.recalculate()
        self.assertEqual(new.arguments, self._init_args)

    def test_documentation_exists(self):
        self.assertTrue(len(self.dist.__doc__) > 100)

    def test_all_number_arguments_converted_to_tensor(self):
        for val in self.dist.arguments.values():
            self.assertNotIsInstance(val, Number)

    def test_params_are_the_same_after_recalculate(self):
        old_ids = {key: id(par) for key, par in self.dist.named_parameters()}
        self.dist.recalculate()
        new_ids = {key: id(par) for key, par in self.dist.named_parameters()}
        for key in old_ids:
            self.assertEqual(old_ids[key], new_ids[key])

    def test_pickle(self):
        with io.BytesIO() as bytes_io:
            pickle.dump(self.dist, bytes_io)
            bytes_io.seek(0)
            loaded_dist = pickle.load(bytes_io)
            assert isinstance(loaded_dist, self._distribution)


class TestNormal(TestDistribution):
    _distribution = distributions.Normal
    _init_args = {"loc": torch.tensor(1.0), "scale": torch.tensor(1.0)}

    def test_can_draw_sample_with_expanded_shape(self):
        sample = self.dist.sample(sample_shape=(3, 3))
        self.assertEqual(sample.shape, torch.Size([3, 3]))

    def test_support_returns_a_constriant(self):
        self.assertIsInstance(self.dist.support, constraints.Constraint)


class TestBernoulli(TestNormal):
    _distribution = distributions.Bernoulli
    _init_args = {"probs": 1}


class TestPareto(TestNormal):
    _distribution = distributions.Pareto
    _init_args = {"scale": 1, "alpha": 1}


class TestMultinomial(TestNormal):
    _distribution = distributions.Multinomial
    _init_args = {"probs": torch.tensor([1.0, 1.0, 1.0, 1.0])}

    def test_can_draw_sample_with_expanded_shape(self):
        sample = self.dist.sample(sample_shape=(3, 3))
        self.assertEqual(sample.shape, torch.Size([3, 3, 4]))


class TestUniform(TestNormal):
    _distribution = distributions.Uniform
    _init_args = {"low": 1, "high": 2}


class TestBinomial(TestNormal):
    _distribution = distributions.Binomial
    _init_args = {"total_count": 10, "probs": torch.tensor([0, 0.2, 0.8, 1])}

    def test_can_draw_sample_with_expanded_shape(self):
        sample = self.dist.sample(sample_shape=(3, 3))
        self.assertEqual(sample.shape, torch.Size([3, 3, 4]))


class TestLogNormal(TestNormal):
    _distribution = distributions.LogNormal
    _init_args = {"loc": torch.tensor(1.0), "scale": torch.tensor(1.0)}

    def test_backward(self):
        q_dist = distribution_utils.dist_to_qdist_infer_hierarchy(self.dist)
        for _ in range(3):
            q_dist = q_dist.recalculate()
            sample = q_dist.rsample()
            loss = self.dist.log_prob(sample) - q_dist.log_prob(sample)
            loss.backward()


class TestTransformedDistribution(TestNormal):
    _distribution = distributions.TransformedDistribution
    _init_args = {
        "base_distribution": distributions.Uniform(0, 1),
        "transforms": [transforms.ExpTransform()],
        "validate_args": None,
    }

    def test_positive_samples(self):
        self.assertEqual((self.dist.sample(torch.Size([100])) > 0).sum(), 100)

    def test_can_take_single_transform_as_input(self):
        tdist = distributions.TransformedDistribution(
            distributions.Normal(0, 1), transforms.ExpTransform()
        )
        self.assertIsInstance(tdist, distributions.TransformedDistribution)

    def test_lamda_fn_as_transform_gives_value_error(self):
        with self.assertRaises(ValueError):
            distributions.TransformedDistribution(
                distributions.Normal(0, 1), lambda x: x
            )


class TestPointMass(TestDistribution):
    _distribution = distributions.PointMass
    u_tensor = torch.randn(100, requires_grad=True)
    _init_args = {"u_tensor": u_tensor, "support": constraints.real}

    def test_value_is_property(self):
        self.assertIsInstance(type(self.dist).u_tensor, property)

    def test_gradients_propagate_correctly(self):
        self.dist.rsample().sum().backward()
        self.assertIsNotNone(self.u_tensor.grad)

    def test_save_state_as_sample(self):
        self.dist.save_state_as_sample()
        self.assertTrue(torch.allclose(self.dist.sample(), self.dist.sample()))
        self.assertIsNone(self.dist.sample().grad)

    def test_sample_when_no_samples_are_stores(self):
        with self.assertRaises(IndexError):
            self.dist.sample(index=10)

    def test_support(self):
        self.assertIsInstance(self.dist.support, type(constraints.real))

    def test_arg_constraints(self):
        self.assertIsInstance(self.dist.arg_constraints, dict)
        self.assertEqual(len(self.dist.arg_constraints), 1)

    def test_arguments_are_correctly_recorded(self):
        self.assertEqual(self.dist.arguments, self._init_args)

    def test_init_with_scalar(self):
        pmd = distributions.PointMass(1, constraints.real)
        self.assertIsInstance(pmd, distributions.Distribution)

    def test_transformed_param_as_u_tensor_raises_error(self):
        with self.assertRaises(RuntimeError):
            distributions.PointMass(
                TP(torch.exp, torch.randn(1)), dist.constraints.real
            )


class Test_Delta(TestDistribution):
    _distribution = distributions.Delta
    value = torch.randn(5, requires_grad=True)
    _init_args = {"value": value, "event_shape": torch.Size()}

    def test_value_is_property(self):
        self.assertIsInstance(type(self.dist).value, property)

    def test_gradients_propagate_correctly(self):
        self.dist.rsample().sum().backward()
        self.assertIsNotNone(self.value.grad)

    def test_support(self):
        self.assertIsInstance(self.dist.support, type(constraints.real))

    def test_arg_constraints(self):
        self.assertIsInstance(self.dist.arg_constraints, dict)
        self.assertEqual(len(self.dist.arg_constraints), 1)

    def test_arguments_are_correctly_recorded(self):
        self.assertEqual(self.dist.arguments, self._init_args)

    def test_init_with_scalar(self):
        pmd = distributions.Delta(1, constraints.real)
        self.assertIsInstance(pmd, distributions.Distribution)

    def test_log_prob_returns_inf_if_summed(self):
        x = torch.ones(5)
        self.assertTrue(np.array(self.dist.log_prob(x).sum().abs()) == np.inf)

    def test_log_prob_returns_zero_if_all_equal(self):
        x = torch.ones(5)
        test_dist = distributions.Delta(x)
        self.assertTrue(np.array(test_dist.log_prob(x).sum()) == 0)

    def test_propper_vi_grad_elbo(self):
        par = torch.ones(5, requires_grad=True)
        q_dist = distributions.Delta(par)
        p_dist = distributions.Normal(0, 1)
        x = q_dist.rsample()
        (p_dist.log_prob(x) - q_dist.log_prob(x)).sum().backward()
        self.assertEqual(float(par.grad.sum()), -5)

    def test_mean_returns_correct_value(self):
        test_dist = distributions.Delta(1)
        self.assertEqual(float(test_dist.mean), 1)

    def test_variance_returns_correct_value(self):
        test_dist = distributions.Delta(1)
        self.assertEqual(float(test_dist.variance), 0)

    def test_sample_does_not_require_grad(self):
        par = torch.ones(5, requires_grad=True)
        test_dist = distributions.Delta(par)
        self.assertFalse(test_dist.sample().requires_grad)

    def test_paramaters_are_only_tensors(self):
        for par in self.dist.parameters():
            self.assertIsInstance(par, torch.Tensor)


if __name__ == "__main__":
    import unittest

    unittest.main()
