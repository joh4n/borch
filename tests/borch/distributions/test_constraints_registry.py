from unittest import TestCase
import torch
from numpy import testing as npt

from borch.distributions import constraints, constraints_registry as cr
from borch.utils.torch_utils import get_device

# pylint: disable=unused-argument
class TestConstraintRegistry(TestCase):
    @staticmethod
    def get_constraints():
        tensor = torch.Tensor
        return [
            constraints.real,
            constraints.positive,
            constraints.GreaterThan(tensor([-10.0, -2, 0, 2, 10]).to(get_device())),
            constraints.GreaterThan(0),
            constraints.GreaterThan(2),
            constraints.GreaterThan(-2),
            constraints.GreaterThanEq(0),
            constraints.GreaterThanEq(2),
            constraints.GreaterThanEq(-2),
            constraints.LessThan(tensor([-10.0, -2, 0, 2, 10]).to(get_device())),
            constraints.LessThan(0),
            constraints.LessThan(2),
            constraints.LessThan(-2),
            constraints.unit_interval,
            constraints.Interval(
                tensor([-4.0, -2, 0, 2, 4]).to(get_device()),
                tensor([-3.0, 3, 1, 5, 5]).to(get_device()),
            ),
            constraints.Interval(-2, -1),
            constraints.Interval(1, 2),
            constraints.HalfOpenInterval(
                tensor([-4.0, -2, 0, 2, 4]).to(get_device()),
                tensor([-3.0, 3, 1, 5, 5]).to(get_device()),
            ),
            constraints.HalfOpenInterval(-2, -1),
            constraints.HalfOpenInterval(1, 2),
            constraints.simplex,
            constraints.lower_cholesky,
        ]

    def test_biject_to(self):
        for constraint in self.get_constraints():
            try:
                transform = cr.biject_to(constraint)
            except NotImplementedError:
                continue
            self.assertTrue(transform.bijective)
            x = torch.randn(5, 5).to(get_device())
            y = transform(x)
            self.assertTrue(constraint.check(y).all())
            x_2 = transform.inv(y)
            npt.assert_allclose(x.cpu(), x_2.cpu(), atol=1e-5, rtol=1e-5)
            j = transform.log_abs_det_jacobian(x, y)
            self.assertEqual(j.shape, x.shape[: x.dim() - transform.event_dim])

    def test_transform_to(self):
        for constraint in self.get_constraints():
            transform = cr.transform_to(constraint)
            x = torch.randn(5, 5).to(get_device())
            y = transform(x)
            self.assertTrue(constraint.check(y).all())
            x_2 = transform.inv(y)
            y_2 = transform(x_2)
            npt.assert_allclose(y.cpu(), y_2.cpu(), atol=1e-6, rtol=1e-6)

    def test_register_class_that_does_not_hinerit_from_constraints(self):
        with self.assertRaises(TypeError):

            @cr.transform_to.register(bool)
            def some_fn():  # pylint: disable=unused-variable
                pass

    def test_transform_to_cat_and_stack(self):
        for con in [constraints.Stack, constraints.Cat]:
            constraint = con([constraints.Positive(), constraints.Real()])
            out = cr.transform_to(constraint)(torch.randn(2, 20))
            self.assertTrue(all(out[0, :] > 0))
            self.assertTrue(min(out[1, :]) < 0)
            self.assertTrue(max(out[1, :]) > 0)

    def test_biject_to_cat(self):
        for con in [constraints.Stack, constraints.Cat]:
            constraint = con([constraints.Positive(), constraints.Real()])
            out = cr.biject_to(constraint)(torch.randn(2, 20))
            self.assertTrue(all(out[0, :] > 0))
            self.assertTrue(min(out[1, :]) < 0)
            self.assertTrue(max(out[1, :]) > 0)
