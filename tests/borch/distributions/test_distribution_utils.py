from unittest import TestCase, mock
import pickle
import io

import torch

from borch.tensor import RV, TransformedParameter
from borch import distributions
from borch.distributions import constraints, distribution_utils as dist_utils


def _serilize_and_load(dist):
    with io.BytesIO() as bytes_io:
        pickle.dump(dist, bytes_io)
        bytes_io.seek(0)
        return pickle.load(bytes_io)


class Test_dist_to_qdist_infer_hierarchy_infer_hierarchy(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(loc=0, scale=torch.ones(1))
        self.q_dist = dist_utils.dist_to_qdist_infer_hierarchy(self.dist)

    def test_returns_a_borch_distribution(self):
        self.assertIsInstance(self.q_dist, distributions.Distribution)

    def test_returns_the_same_distribution_type(self):
        self.assertIsInstance(self.q_dist, type(self.dist))

    def test_q_dist_arguments_are_transformed_parameters(self):
        q_dist = self.q_dist
        self.assertIsInstance(q_dist.arguments["loc"], TransformedParameter)
        self.assertIsInstance(q_dist.arguments["scale"], TransformedParameter)

    def test_arguments_are_not_transformed_parameters_when_grad_required(self):
        dist = distributions.Normal(
            loc=torch.tensor([1.0], requires_grad=True),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist_infer_hierarchy(dist)
        self.assertNotIsInstance(q_dist.arguments["loc"], TransformedParameter)
        self.assertNotIsInstance(q_dist.arguments["scale"], TransformedParameter)

    def test_real_constraint_used_when_distribution_is_multinomal(self):
        # todo: I expect that this will fail...
        pass

    def test_can_be_pickled(self):
        dist = distributions.LogNormal(
            loc=torch.tensor([1.0], requires_grad=False),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist_infer_hierarchy(dist)
        loaded_dist = _serilize_and_load(q_dist)
        self.assertIsInstance(loaded_dist, distributions.LogNormal)


class Test_dist_to_qdist(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(loc=0, scale=torch.ones(1))
        self.q_dist = dist_utils.dist_to_qdist(self.dist)

    def test_returns_a_borch_distribution(self):
        self.assertIsInstance(self.q_dist, distributions.Distribution)

    def test_returns_the_same_distribution_type(self):
        self.assertIsInstance(self.q_dist, type(self.dist))

    def test_q_dist_arguments_are_transformed_parameters(self):
        q_dist = self.q_dist
        self.assertIsInstance(q_dist.arguments["loc"], TransformedParameter)
        self.assertIsInstance(q_dist.arguments["scale"], TransformedParameter)

    def test_arguments_are_transformed_parameters_when_grad_required(self):
        dist = distributions.Normal(
            loc=torch.tensor([1.0], requires_grad=True),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist(dist)
        self.assertIsInstance(q_dist.arguments["loc"], TransformedParameter)
        self.assertIsInstance(q_dist.arguments["scale"], TransformedParameter)

    def test_can_be_pickled(self):
        dist = distributions.LogNormal(
            loc=torch.tensor([1.0], requires_grad=True),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist(dist)
        loaded_dist = _serilize_and_load(q_dist)
        self.assertIsInstance(loaded_dist, distributions.LogNormal)


class Test_scaled_normal_dist_from_rv(TestCase):
    def setUp(self):
        self.rv = RV(distributions.Cauchy(3, 3))
        self.new = dist_utils.scaled_normal_dist_from_rv(self.rv, -2)

    def test_raises_type_error_input_rv_does_not_have_real_support(self):
        rv = RV(distributions.Gamma(1, 1))
        with self.assertRaises(TypeError):
            dist_utils.scaled_normal_dist_from_rv(rv, 0.01)

    def test_arguments_are_transformed_parameters(self):
        for arg in self.new.arguments.values():
            self.assertIsInstance(arg, TransformedParameter)

    def test_arguments_are_optimisable(self):
        for arg in self.new.arguments.values():
            for param in arg.parameters():
                self.assertTrue(param.requires_grad)

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        # the mean of the normal distribution will be a sample from the rv
        # if the field rv.data is not finite
        self.rv.data /= 0
        with mock.patch.object(
            self.rv.distribution, "sample", wraps=self.rv.distribution.sample
        ) as mocked:
            dist_utils.scaled_normal_dist_from_rv(self.rv, -2)
        mocked.assert_called_once()


class TestGetConstraint(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(1, 1)

    def test_returns_real_when_name_is_logits(self):
        ans = dist_utils._get_constraint("logits", self.dist)
        self.assertIs(constraints.real, ans)

    def test_raises_lookup_error_if_name_not_in_arg_constraints(self):
        with self.assertRaises(LookupError):
            dist_utils._get_constraint("x", self.dist)


class TestNormalDistributionFromRV(TestCase):
    def setUp(self):
        self.rv = RV(distributions.Cauchy(3, 3))

    # def test_raises_type_error_input_rv_does_not_have_real_support(self):
    # todo enable this when we have a way to detect the error
    #     rv = RV(distributions.Poisson(4))
    #     with self.assertRaises(TypeError):
    #         dist_utils.normal_distribution_from_rv(rv, -3.)

    def test_arguments_are_transformed_parameters(self):
        new = dist_utils.normal_distribution_from_rv(self.rv, -2)
        for arg in new.arguments.values():
            self.assertIsInstance(arg, TransformedParameter)

    def test_arguments_are_optimisable(self):
        new = dist_utils.normal_distribution_from_rv(self.rv, -2)
        for arg in new.arguments.values():
            for param in arg.parameters():
                self.assertTrue(param.requires_grad)

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        # the mean of the normal distribution will be a sample from the rv
        # if the field rv.data is not finite
        self.rv.data /= 0
        with mock.patch.object(
            self.rv.distribution, "sample", wraps=self.rv.distribution.sample
        ) as mocked:
            dist_utils.normal_distribution_from_rv(self.rv, -2)
        mocked.assert_called_once()

    def test_gamma(self):
        rv = RV(distributions.Gamma(1, 1))
        new = dist_utils.normal_distribution_from_rv(rv, -3.0)
        self.assertIsInstance(new, distributions.TransformedDistribution)

    def test_gamma_optimizable_paramaters(self):
        rv = RV(distributions.Gamma(1, 1))
        new = dist_utils.normal_distribution_from_rv(rv, -3.0)
        self.assertEqual(len(list(RV(new).parameters())), 2)


class Test_delta_distribution_from_rv(TestCase):
    def setUp(self):
        self.rv = RV(distributions.Cauchy(3, 3))

    def test_arguments_requires_grad(self):
        new = dist_utils.delta_distribution_from_rv(self.rv)
        self.assertTrue(new.value.requires_grad)

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        # the mean of the normal distribution will be a sample from the rv
        # if the field rv.data is not finite
        self.rv.data /= 0
        with mock.patch.object(
            self.rv.distribution, "sample", wraps=self.rv.distribution.sample
        ) as mocked:
            dist_utils.delta_distribution_from_rv(self.rv)
        mocked.assert_called_once()

    def test_gamma(self):
        rv = RV(distributions.Gamma(1, 1))
        new = dist_utils.delta_distribution_from_rv(rv)
        self.assertIsInstance(new, distributions.TransformedDistribution)

    def test_gamma_optimizable_paramaters(self):
        rv = RV(distributions.Gamma(1, 1))
        new = dist_utils.delta_distribution_from_rv(rv)
        self.assertEqual(len(list(RV(new).parameters())), 1)


if __name__ == "__main__":
    import unittest

    unittest.main()
