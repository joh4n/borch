from unittest import TestCase, mock

import torch

from borch import nn
from borch.nn.utils import borchify
from borch.guide import NormalGuide

from borch.rv_factories import parameter_to_normal_rv


class TestBorchifyLinearModule(TestCase):
    _cls = torch.nn.Linear
    _in_features = 2
    _out_features = 2
    _init_args = (_in_features, _out_features)
    _init_kwargs = {}

    def setUp(self):
        self.module = self._cls(*self._init_args, **self._init_kwargs)
        self.borchified = borchify.borchify_module(self.module)

    def test_borchified_module_inherits_from_original_torch_module(self):
        self.assertIsInstance(self.borchified, self._cls)

    def test_inherits_from_borch_module(self):
        self.assertIsInstance(self.borchified, nn.Module)

    def test_borchified_object_is_unique(self):
        self.assertNotEqual(id(self.module), id(self.borchified))

    def test_original_object_retains_id(self):
        id_before = id(self.module)
        borchify.borchify_module(self.module)
        id_after = id(self.module)
        self.assertEqual(id_before, id_after)

    def test_borchified_object_has_random_variables(self):
        self.assertTrue(bool(tuple(self.borchified.random_variables())))

    def test_changes_in_module_dont_affect_borch_module(self):
        param = torch.nn.Parameter(torch.ones(1))
        self.module.register_parameter("test", param)
        self.assertIn(id(param), map(id, self.module.parameters()))
        self.assertNotIn(id(param), map(id, self.borchified.parameters()))

    def test_changes_in_borch_module_dont_affect_module(self):
        param = torch.nn.Parameter(torch.ones(1))
        # Note that by using `register_parameter` directly, `param` is not
        # added as a random variable.
        self.borchified.register_parameter("test", param)
        self.assertIn(id(param), map(id, self.borchified.parameters()))
        self.assertNotIn(id(param), map(id, self.module.parameters()))

    def test_calls_get_rv_factory_when_no_rv_factory_provided(self):
        with mock.patch.object(borchify, "get_rv_factory") as mocked:
            borchify.borchify_module(self._cls)
        mocked.assert_called()

    def test_doea_not_call_get_rv_factory_when_rv_factory_provided(self):
        with mock.patch.object(borchify, "get_rv_factory") as mocked:
            borchify.borchify_module(self._cls, rv_factory=parameter_to_normal_rv)
        self.assertFalse(mocked.called)


class ExampleNetwork(torch.nn.Module):
    """A test network for checking borchification."""

    def __init__(self):
        super().__init__()
        linear = torch.nn.Linear(2, 2)
        linear.add_module("nested", torch.nn.Linear(3, 3))
        linear.add_module("nested2", torch.nn.Linear(4, 4))
        self.linear = linear
        self.conv = torch.nn.Conv1d(1, 1, 3)
        self.flatten = nn.Flatten()

    def forward(self, *inputs):
        return self.linear(inputs[0])


class TestBorchifyNetwork(TestCase):
    def setUp(self):
        self.net = ExampleNetwork()
        self.borchified = borchify.borchify_network(self.net)

    def test_base_level_was_borchified(self):
        self.assertIsInstance(self.borchified, nn.Module)

    def test_linear_was_borchified(self):
        self.assertIsInstance(self.borchified.linear, nn.Module)
        self.assertIsInstance(self.borchified.conv, nn.Module)

    def test_nested_was_borchified(self):
        self.assertIsInstance(self.borchified.linear.nested, nn.Module)

    def test_linear_has_random_variables(self):
        rvs = tuple(self.borchified.linear.random_variables())
        self.assertTrue(bool(rvs))

    def test_nested_has_random_variables(self):
        rvs = tuple(self.borchified.linear.nested.random_variables())
        self.assertTrue(bool(rvs))

    def test_nested2_has_random_variables(self):
        rvs = tuple(self.borchified.linear.nested2.random_variables())
        self.assertTrue(bool(rvs))

    def test_conv_has_random_variables(self):
        rvs = tuple(self.borchified.conv.random_variables())
        self.assertTrue(bool(rvs))

    def test_double_usage_handled_appropriately(self):
        # Here we add `self.linear` as a nested module on `self.conv`. We want
        # to see that `self.linear` and its nested module are NOT re-borchified
        # and added, but instead the already borchified versions in the cache
        # are used.
        self.net.conv.add_module("nested", self.net.linear)

        # assert the setup is correct
        self.assertIs(self.net.conv.nested, self.net.linear)
        self.assertIs(self.net.conv.nested.nested, self.net.linear.nested)

        borchified = borchify.borchify_network(self.net)
        self.assertIs(borchified.conv.nested, borchified.linear)
        self.assertIs(borchified.conv.nested.nested, borchified.linear.nested)

    def test_can_call_forward(self):
        # Only a function call test
        self.borchified(torch.rand(10, 2))

    def test_guide_is_set_correctly_with_supplied_guide_creator(self):
        guide_creator = mock.MagicMock(
            side_effect=lambda *args, **kwargs: NormalGuide()
        )
        new = borchify.borchify_network(self.net, guide_creator=guide_creator)

        # First check that the number of calls was correct, this should be
        # equal to the total number of modules in the network
        n_modules = len(tuple(self.net.modules()))
        self.assertEqual(guide_creator.call_count, n_modules)

        # Then we can check that all of the guides for these modules have been
        # assigned to the return value of the guide_creator
        for mod in new.modules():
            if isinstance(mod, nn.Module):
                self.assertIsInstance(mod.guide, NormalGuide)


if __name__ == "__main__":
    import unittest

    unittest.main()
