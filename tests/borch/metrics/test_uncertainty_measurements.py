from unittest import TestCase

import torch

from borch import RV, distributions
from borch.metrics import uncertainty_measurements as um


class Test_epistemic_uncertainty(TestCase):
    correct_value = [1.4189, 2.5310, 1.0]
    unc_fn = staticmethod(um.epistemic_uncertainty)

    def setUp(self):
        self.rvs = [
            RV(distributions.Normal(0, 1)),
            RV(distributions.Cauchy(0, 1)),
            RV(distributions.Gamma(1, 1)),
            RV(distributions.NegativeBinomial(1, 1)),
        ]

    def test_returns_tensors(self):
        for rv in self.rvs:
            self.assertIsInstance(self.unc_fn(rv), torch.Tensor)

    def test_correct_value_for_normal(self):
        for rv, val in zip(self.rvs, self.correct_value):
            self.assertAlmostEqual(float(self.unc_fn(rv)), val, delta=1e-4)


class Test_het_aleatoric_uncertainty(Test_epistemic_uncertainty):
    correct_value = [1.0, float("inf"), 1.0]
    unc_fn = staticmethod(um.het_aleatoric_uncertainty)
