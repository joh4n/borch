import unittest

import torch

from borch import nn


class Test_View(unittest.TestCase):
    def test_to_minus_one(self):
        view = nn.View(-1)
        out = view(torch.randn(10, 10))
        self.assertEqual(out.size(), torch.Size([100]))

    def test_to_one_minus_one(self):
        view = nn.View(1, -1)
        out = view(torch.randn(10, 1, 1, 1, 10))
        self.assertEqual(out.size(), torch.Size([1, 100]))

    def test_to_minus_one_one(self):
        view = nn.View(-1, 1)
        out = view(torch.randn(1, 10, 10, 1))
        self.assertEqual(out.size(), torch.Size([100, 1]))

    def test_returns_tensor(self):
        view = nn.View(-1, 1)
        out = view(torch.randn(10, 10))
        self.assertIsInstance(out, torch.Tensor)

    def test_repr(self):
        view = nn.View(1, -1)
        self.assertEqual(str(view), "View(1, -1)")


class Test_BatchView(unittest.TestCase):
    def test_to_minus_one(self):
        view = nn.BatchView(-1)
        out = view(torch.randn(10, 10))
        self.assertEqual(out.size(), torch.Size([10, 10]))

    def test_to_one_minus_one(self):
        view = nn.BatchView(-1)
        out = view(torch.randn(10, 1, 1, 1, 10))
        self.assertEqual(out.size(), torch.Size([10, 10]))

    def test_to_minus_one_one(self):
        view = nn.BatchView(-1, 1)
        out = view(torch.randn(1, 10, 10, 1))
        self.assertEqual(out.size(), torch.Size([1, 100, 1]))

    def test_returns_tensor(self):
        view = nn.BatchView(-1, 1)
        out = view(torch.randn(10, 10))
        self.assertIsInstance(out, torch.Tensor)

    def test_repr(self):
        view = nn.BatchView(1, -1)
        self.assertEqual(str(view), "BatchView(1, -1)")


class Test_Flatten(unittest.TestCase):
    def test_10by10(self):
        view = nn.Flatten()
        out = view(torch.randn(10, 10))
        self.assertEqual(out.size(), torch.Size([10, 10]))

    def test_many_one_dims(self):
        view = nn.Flatten()
        out = view(torch.randn(10, 1, 1, 1, 10))
        self.assertEqual(out.size(), torch.Size([10, 10]))

    def test_many_dims(self):
        view = nn.Flatten()
        out = view(torch.randn(5, 10, 10, 3))
        self.assertEqual(out.size(), torch.Size([5, 300]))

    def test_repr(self):
        view = nn.Flatten()
        self.assertEqual(str(view), "Flatten()")


class Test_Expand(unittest.TestCase):
    def test_basic_usage(self):
        expand = nn.Expand(10, 10, 10)
        out = expand(torch.randn(10, 10))
        self.assertEqual(out.size(), torch.Size([10, 10, 10]))


class Test_ExpandBatch(unittest.TestCase):
    def test_basic_usage(self):
        expand = nn.ExpandBatch(-1, 3)
        out = expand(torch.randn(5, 10, 1))
        self.assertEqual(out.size(), torch.Size([5, 10, 3]))
