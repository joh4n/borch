from io import BytesIO
from tempfile import NamedTemporaryFile
import pickle as pkl
from unittest import TestCase, mock

import torch
from torch.nn import Parameter

from borch import RV, TP, distributions, rv_factories
from borch import Module, guide
from borch.nn import torch_proxies
from borch.infer import vi_loss


class TestLinear(TestCase):
    in_features = 3
    out_features = 3
    _cls = torch_proxies.Linear
    _init_args = (in_features, out_features)
    _init_kwargs = {}

    def setUp(self):
        self.module = self._cls(*self._init_args, **self._init_kwargs)
        self.mu = TP(lambda x: x, Parameter(torch.Tensor([1, 2, 3.0])))
        self.sd = TP(lambda x: x, Parameter(torch.Tensor([1, 1, 1.0])))
        self.rv = RV(distributions.Normal(self.mu, self.sd))

    def test_correct_scale_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.scale.mean().item(), 0.57735, delta=0.0001
        )

    def test_setting_prior_for_weight_and_bias_large_mean(self):
        module = torch_proxies.Linear(
            5,
            5,
            weight=distributions.Normal(100, 0.1),
            bias=distributions.Normal(100, 0.1),
        )
        self.assertTrue(module.prior.weight.mean() > 99)

        self.assertTrue(module.prior.bias.mean() > 99)

    def test_setting_prior_gamma_mean(self):
        module = torch_proxies.Linear(40, 40, weight=distributions.Gamma(1, 1))
        self.assertAlmostEqual(float(module.prior.weight.mean()), 1, delta=0.1)

    def test_setting_prior_gamma_forward(self):
        module = torch_proxies.Linear(
            2, 2, bias=False, weight=distributions.Gamma(1, 1)
        )
        self.assertGreater(module(torch.randn(1, 2).abs()).sum(), 0)

    def test_setting_prior_categorical_forward(self):
        module = torch_proxies.Linear(
            2,
            2,
            bias=False,
            weight=distributions.Categorical(logits=torch.randn(2, 2, 2)),
            guide=guide.AutomaticGuide(),
        )
        self.assertGreaterEqual(module(torch.randn(1, 2).abs().long()).sum(), 0)

    def test_setting_prior_with_non_valid_brodcast_shape_raises_error(self):
        with self.assertRaises(RuntimeError):
            torch_proxies.Linear(
                2,
                2,
                bias=False,
                weight=distributions.Categorical(logits=torch.randn(2, 2)),
                guide=guide.AutomaticGuide(),
            )

    def test_setting_prior_negative_binomial_forward(self):
        module = torch_proxies.Linear(
            2,
            2,
            bias=False,
            weight=distributions.NegativeBinomial(1, logits=2),
            guide=guide.AutomaticGuide(),
        )
        self.assertGreater(module(torch.randn(1, 2).abs()).sum(), 0)

    def test_setting_prior_for_weight_large_std(self):
        module = torch_proxies.Linear(5, 5, weight=distributions.Normal(0, 100))
        self.assertTrue(module.prior.weight.std() > 50)

    def test_setting_prior_for_not_valid_name(self):
        with self.assertRaises(ValueError):
            torch_proxies.Linear(5, 5, bad_name=distributions.Normal(0, 100))

    def test_correct_loc_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.loc.mean().item(), 0, delta=0.0001
        )

    def test_weight_is_a_random_variable(self):
        self.assertIsInstance(self.module["weight"], RV)

    def test_bias_is_a_random_variable(self):
        self.assertIsInstance(self.module["bias"], RV)

    def test_borch_call_calls_torch_forward(self):
        # todo: generalise
        with mock.patch.object(torch.nn.Linear, "forward") as mocked:
            self.module(torch.ones(4, self.in_features))
        mocked.assert_called()

    def test_can_add_parameter_to_module(self):
        name = "param"
        setattr(self.module, name, torch.nn.Parameter(torch.ones(3)))
        self.assertIsInstance(getattr(self.module, name), torch.nn.Parameter)

    @mock.patch("borch.tensor.RV.rsample")
    def test_weights_rsample_not_called_with_sampling_off(self, mocked):
        self.module.sampling(False)
        _ = self.module.weight
        mocked.assert_not_called()

    @mock.patch("borch.tensor.RV.rsample")
    def test_weights_rsample_called_with_sampling_on(self, mocked):
        self.module.sampling(True)
        _ = self.module.weight
        mocked.assert_called()

    def test_zero_grad_zeroes_gradients_of_all_parameters(self):
        self.module.sampling(True)

        # produce gradients
        self.module(torch.ones(4, self.in_features)).sum().backward()

        # zero them
        self.module.zero_grad()

        # check gradients are 0
        for par in self.module.opt_parameters():
            self.assertTrue(torch.equal(par.grad, torch.zeros_like(par.grad)))

    def test_correct_number_optimisable_tensors(self):
        self.assertEqual(len(list(self.module.opt_parameters())), 4)

    def test_gradients_are_none_before_backward_call(self):
        for param in self.module.parameters():
            self.assertIsNone(param.grad)

    def test_gradients_propagate_with_sampling_on(self):
        self.module.sampling(True)  # NB very important!
        self.module(torch.ones(4, self.in_features)).sum().backward()
        for param in self.module.opt_parameters():
            self.assertIsNotNone(param.grad)
        self.assertEqual(len(list(self.module.opt_parameters())), 4)

    def test_network_can_be_propagated_after_casting_to_double(self):
        img = torch.randn(self.in_features)
        self.module.double()
        self.module.sample()
        self.module(img.double())

    def test_gradients_dont_propagate_to_sd_without_sampling(self):
        self.module(torch.ones(4, self.in_features)).sum().backward()
        for rv in self.module.random_variables():
            grad = rv.distribution.scale.grad
            self.assertIsNone(grad)

    def test_parameters_calls_named_parameters_on_all_random_vars(self):
        for rv in self.module.random_variables():
            rv.named_parameters = mock.create_autospec(rv.named_parameters)

        tuple(self.module.parameters())  # NB force evaluation

        for rv in self.module.random_variables():
            rv.named_parameters.assert_called_once()

    def test_borch_linear_contains_twice_the_params_as_original(self):
        # todo: this is a fluffy test... it depends on RV
        linear = torch.nn.Linear(self.in_features, self.out_features)
        orig_params = tuple(linear.parameters())
        # parameters() returns also tensors from the prior
        new_params = tuple(self.module.opt_parameters())
        self.assertEqual(len(new_params), 2 * len(orig_params))

    def test_setattr_parameter_adds_a_new_parameter(self):
        param = torch.nn.Parameter(torch.ones(3))
        self.module.new_param = param
        self.assertIs(self.module.new_param, param)

    def test_parameter_can_be_registered_using_register_parameter(self):
        param = torch.nn.Parameter(torch.ones(1))
        # Note that by using `register_parameter` directly, `param` is not
        # added as a random variable.
        self.module.register_parameter("test", param)
        self.assertIn(id(param), map(id, self.module.parameters()))

    def test_random_variables_returns_correct_n_rvs(self):
        # This function tests that random variables are yeilded from
        # ppl modules three layers deep.
        new_linear1 = torch_proxies.Linear(1, 1)
        new_linear2 = torch_proxies.Linear(1, 1)
        new_linear3 = torch_proxies.Linear(1, 1)

        new_linear2.add_module("nested3", new_linear3)
        new_linear1.add_module("nested2", new_linear2)
        self.module.add_module("nested1", new_linear1)

        self.assertEqual(len(tuple(self.module.random_variables())), 16)

    def test_random_variables_returns_all_unique_objects(self):
        new_linear = torch_proxies.Linear(1, 1)
        self.module.add_module("name1", new_linear)
        self.module.add_module("name2", new_linear)
        rvs = tuple(self.module.random_variables())
        self.assertEqual(len(set(rvs)), len(rvs))

    def test_can_be_saved(self):
        with NamedTemporaryFile("wb") as f:
            sd = torch_proxies.Linear(self.in_features, self.out_features).state_dict()
            pkl.dump(sd, f)


class TestConv2d(TestCase):
    def setUp(self):
        self.module = torch_proxies.Conv2d(1, 1, 3, 1)

    def test_network_can_be_propagated_after_casting_to_double(self):
        img = torch.randn((2, 1, 28, 28))
        self.module.double()
        self.module.sample()
        self.module(img.double())

    def test_correct_scale_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.scale.mean().item(), 0.333333, delta=0.0001
        )

    def test_correct_loc_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.loc.mean().item(), 0, delta=0.0001
        )


class Test_BatchNorm2d(TestCase):
    def setUp(self):
        self.module = torch_proxies.BatchNorm2d(3)

    # old tests for baysian batchnorm
    #     def test_correct_scale_of_prior(self):
    #         self.assertAlmostEqual(
    #             self.module.prior.weight.scale.mean().item(), 1.0, delta=0.0001
    #         )
    #
    #     def test_correct_loc_of_prior(self):
    #         self.assertAlmostEqual(
    #             self.module.prior.weight.loc.mean().item(), 0, delta=0.0001
    #         )

    def test_no_rvs_for_batch_norm(self):
        for par in self.module.parameters():
            self.assertFalse(isinstance(par, RV))


class Net(Module):
    def __init__(self):
        super().__init__()
        self.fc1 = torch_proxies.Linear(3, 3)
        self.fc2 = torch_proxies.Linear(3, 3)

    def forward(self, *inputs):
        return torch.sigmoid(self.fc2(torch.sigmoid(self.fc1(inputs[0]))))


class TestFullNetwork(TestCase):
    # todo: this is an integration test, move when appropriate
    _cls = Net

    def setUp(self):
        self.net = self._cls()

    def test_gradients_propagate_for_entire_network(self):
        self.net.sampling(True)
        loss = vi_loss(**self.net.pq_to_infer())
        loss.backward()
        for param in self.net.opt_parameters():
            self.assertIsNotNone(param.grad)

    def test_correct_number_of_paramaters(self):
        self.assertEqual(len(list(self.net.opt_parameters())), 8)

    def test_network_can_be_serialised(self):
        f = BytesIO()
        torch.save(self.net.state_dict(), f)
        f.seek(0)

        net2 = Net()
        net2.load_state_dict(torch.load(f))
        self.assertTrue(torch.equal(net2.fc1.weight, self.net.fc1.weight))

        self.assertTrue(torch.equal(net2.fc1.guide.weight, self.net.fc1.guide.weight))


class Test_get_rv_factory(TestCase):
    def test_returns_callable(self):
        rv_factory = torch_proxies.get_rv_factory("Module without default guide")
        self.assertTrue(callable(rv_factory))

    def test_returns_correct_rv_factory_for_linear(self):
        rv_factory = torch_proxies.get_rv_factory("Linear")
        self.assertEqual(rv_factory, rv_factories.kaiming_normal_rv)


class Test_get_guide_factory(TestCase):
    def test_returns_callable(self):
        guid = torch_proxies.get_guide("some_random_string_1")
        self.assertTrue(callable(guid))
        self.assertEqual(guid, guide.NormalGuide)

    def test_returns_correct_guide_for_batchnorm(self):
        guid = torch_proxies.get_guide("BatchNorm2d")
        self.assertEqual(guid, guide.NormalGuide)

    def test_returns_correct_rv_factory_for_linear(self):
        guid = torch_proxies.get_guide("Linear")
        self.assertEqual(guid, guide.NormalGuide)


if __name__ == "__main__":
    import unittest

    unittest.main()
