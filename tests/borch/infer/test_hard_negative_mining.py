from unittest import TestCase

import torch

from borch.utils.torch_utils import one_hot
from borch.infer import hard_negative_mining


class Test_hard_negative_mining(TestCase):
    def test_correctness(self):
        torch.manual_seed(1)
        losses = torch.rand(10, 5)
        labels = one_hot(torch.randint(0, 5, (10,)), n_classes=5)
        mask = hard_negative_mining(losses, labels, 3)
        expected = torch.tensor(
            [
                [1, 0, 1, 1, 1],
                [1, 0, 1, 1, 1],
                [1, 1, 1, 1, 0],
                [1, 1, 0, 1, 1],
                [1, 1, 1, 0, 1],
                [0, 1, 1, 1, 1],
                [1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1],
                [1, 1, 1, 1, 0],
                [1, 0, 1, 1, 1],
            ],
            dtype=torch.uint8,
        ).to(bool)
        self.assertTrue(torch.equal(mask, expected))
