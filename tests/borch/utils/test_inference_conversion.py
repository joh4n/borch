"""
tests
"""
import unittest

import torch

from borch.tensor import RandomVariable
from borch import distributions as dist
from borch.utils.inference_conversion import pq_dict_to_lists, rsample_else_sample
from borch.infer.vi import vi_loss


class Test_pq_dict_to_lists(unittest.TestCase):
    def setUp(self):
        self.correct_keys = ["p_dists", "q_dists", "values", "observed"]
        self.rvar_dict = {
            RandomVariable(dist.Cauchy(0, 1)): RandomVariable(dist.Normal(0, 2))
            for _ in range(5)
        }
        self.lists_dict = pq_dict_to_lists(self.rvar_dict)

    def test_creates_dict(self):
        self.assertIsInstance(self.lists_dict, dict)

    def test_creates_dict_with_tensor_as_key(self):
        rvar_dict = {
            torch.randn(1): RandomVariable(dist.Normal(0, 2)) for _ in range(5)
        }
        self.assertRaises(ValueError, lambda: pq_dict_to_lists(rvar_dict))

    def test_creates_dict_with_tensor_as_value(self):
        rvar_dict = {
            RandomVariable(dist.Normal(0, 2)): torch.randn(1) for _ in range(5)
        }
        self.assertRaises(ValueError, lambda: pq_dict_to_lists(rvar_dict))

    def test_correct_number_of_keys(self):
        self.assertEqual(len(self.lists_dict.keys()), len(self.correct_keys))

    def test_correct_key_name(self):
        for key in self.lists_dict:
            self.assertTrue(key in self.correct_keys)

    def test_integrates_with_infer(self):
        self.assertIsInstance(vi_loss(**self.lists_dict), torch.Tensor)

    def test_correct_number_of_observed(self):
        rvar_dict = {
            RandomVariable(dist.Cauchy(0, 1)): RandomVariable(dist.Normal(0, 2))
            for _ in range(5)
        }
        rvar_dict[RandomVariable(dist.Cauchy(0, 1))]: RandomVariable(
            dist.Normal(0, 2)
        ).observe(torch.ones(1))
        lists_dict = pq_dict_to_lists(rvar_dict)
        not_observed = [val for val in lists_dict["observed"] if val is False]
        self.assertEqual(len(not_observed), 5)


class Test_rsample_else_sample(unittest.TestCase):
    def setUp(self):
        self.par1 = torch.ones(1, requires_grad=True)
        self.par2 = torch.ones((1, 1), requires_grad=True)

    def test_sample_normal(self):
        rvar = RandomVariable(dist.Normal(self.par1, self.par2))
        sample = rsample_else_sample(rvar)
        self.assertIsNotNone(sample.grad_fn)

    def test_sample_bernoulli(self):
        rvar = RandomVariable(dist.Bernoulli(self.par2))
        sample = rsample_else_sample(rvar)
        self.assertIsNone(sample.grad_fn)


if __name__ == "__main__":
    unittest.main()
