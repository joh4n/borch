from unittest import TestCase, mock
from io import BytesIO
import pickle

import torch
from torch.nn import Parameter

import borch.distributions as dist
from borch import nn, RV
from borch import Module, guide


class TestAutomaticGuide(TestCase):
    _guide_cls = guide.AutomaticGuide

    def setUp(self):
        self.guide = self._guide_cls()
        self.normal = dist.Normal(1, 1)
        self.rv = RV(self.normal)
        self.guide.rv = self.rv

    def test_applying_double(self):
        self.guide.double()

    def test_rv_is_cloned_when_added(self):
        self.assertIsNot(self.guide["rv"], self.rv)

    def test_arguments_are_optimisable_for_added_rv(self):
        for arg in self.guide["rv"].distribution.arguments.values():
            if isinstance(arg, torch.Tensor):
                self.assertTrue(arg.requires_grad)

    def test_second_rv_with_same_name_is_not_added_when_using_call(self):
        new = RV(self.normal)
        with mock.patch.object(self.guide, "register_random_variable") as mck:
            self.guide("rv", new)
        mck.assert_not_called()

    def test_getattr_returns_tensor_which_requires_grad(self):
        self.assertTrue(self.guide.rv.requires_grad)

    def test_getitem_same_as_getattr(self):
        self.assertEqual(id(self.guide.rv), id(self.guide["rv"]))

    def test_observed_value_gets_used_in_getattr(self):
        new = RV(self.normal)
        observed = torch.tensor(100.0)
        self.guide.observe(rv=observed)
        self.guide("rv", new)
        self.assertTrue(torch.equal(self.guide.rv, observed))

    def test_calling_does_not_call_rsample_on_rvs(self):
        # rsample is expected to be called given that self["rv"].has_rsample
        # is True and "rsample_else_sample" is used.
        with mock.patch.object(self.guide["rv"], "rsample") as mocked:
            self.guide("rv", self.rv)
        mocked.assert_not_called()

    def _test_thing_can_be_added(self, creator):
        # Since `Guide` overrides the init method of `Module` (with
        # `random_variables_factories` omitted) we should be able to add
        # regular `Parameter` and `Variable` objects to the guide without
        # interference.
        thing = creator(torch.tensor([100.0], requires_grad=True))
        self.guide.thing = thing
        self.assertIs(self.guide.thing, thing)
        self.assertNotIn(thing, self.guide.random_variables())

    def test_torch_parameters_can_be_added_without_interference(self):
        self._test_thing_can_be_added(creator=Parameter)

    def test_torch_tensor_can_be_added_without_interference(self):
        self._test_thing_can_be_added(creator=lambda x: x)

    def test_calling_forward_raises_runtime_error(self):
        with self.assertRaises(RuntimeError):
            self.guide.forward()

    def test_create_ful_model(self):
        model = Module(guide=self._guide_cls())
        for _ in range(3):
            model.sample()
            model.mu = RV(dist.Normal(1, torch.randn(1, requires_grad=True)))
            (-model.get_rv("mu").log_prob(model.mu)).backward()

    def test_manually_specifying_q_dist(self):
        model = Module(guide=self._guide_cls())
        mean = torch.randn(1, requires_grad=True)
        log_sd = torch.randn(1, requires_grad=True)
        model.mu = RV(dist.Normal(1, 1), dist.Normal(mean, log_sd.exp()))
        (-model.prior.mu.log_prob(model.mu) + model.mu.log_prob(model.mu)).backward()
        self.assertIsNotNone(mean.grad)
        self.assertIsNotNone(log_sd.grad)

    def test_can_be_pickled(self):
        f = BytesIO()
        pickle.dump(self.guide, f)
        f.seek(0)
        pickle.load(f)

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        with self.assertWarns(UserWarning):
            with torch.no_grad():
                model = Module(guide=self._guide_cls())
                model.mu = RV(dist.Normal(1, torch.randn(1, requires_grad=True)))


class TestNormalGuide(TestAutomaticGuide):
    _guide_cls = guide.NormalGuide

    def test_adding_cauchy_rv_adds_a_normal_q_rv(self):
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        self.assertIsInstance(self.guide["rv2"].distribution, dist.Normal)

    def test_adding_gamma_rv_adds_a_transformed_dist_q_rv(self):
        rv = RV(dist.Gamma(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        self.assertIsInstance(
            self.guide["rv2"].distribution, dist.TransformedDistribution
        )

    def test_approximating_distribution_arguments_are_leaf_nodes(self):
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        # todo: should this be a test for all guides?
        for rv in self.guide.random_variables():
            for param in rv.parameters():
                self.assertTrue(param.is_leaf)

    def test_manually_specifying_q_dist(self):
        ## we dont expect this to work
        model = Module(guide=self._guide_cls())
        mean = torch.randn(1, requires_grad=True)
        log_sd = torch.randn(1, requires_grad=True)
        model.mu = RV(dist.Normal(1, 1), dist.Normal(mean, log_sd.exp()))
        (-model.prior.mu.log_prob(model.mu) + model.mu.log_prob(model.mu)).backward()
        self.assertIsNone(mean.grad)
        self.assertIsNone(log_sd.grad)

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        pass


class TestScaledNormalGuide(TestNormalGuide):
    _guide_cls = guide.ScaledNormalGuide

    def test_adding_gamma_rv_raises_type_error(self):
        rv = RV(dist.Gamma(torch.ones(3), torch.ones(3)))
        with self.assertRaises(TypeError):
            self.guide("rv2", rv)

    def test_adding_cauchy_rv_adds_a_normal_q_rv(self):
        pass

    def test_adding_gamma_rv_adds_a_transformed_dist_q_rv(self):
        pass


class TestDeltaGuide(TestAutomaticGuide):
    _guide_cls = guide.DeltaGuide

    def test_adding_cauchy_rv_adds_a_delta_q_rv(self):
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        self.assertIsInstance(self.guide["rv2"].distribution, dist.Delta)

    def test_approximating_distribution_arguments_are_leaf_nodes(self):
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        for param in self.guide.parameters():
            self.assertTrue(param.is_leaf)

    def test_correct_n_paramaters(self):
        test_guide = self._guide_cls()
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        test_guide("rv2", rv)
        self.assertEqual(len(list(test_guide.parameters())), 1)

    def test_manually_specifying_q_dist(self):
        ## we dont expect this to work
        model = Module(guide=self._guide_cls())
        mean = torch.randn(1, requires_grad=True)
        log_sd = torch.randn(1, requires_grad=True)
        model.mu = RV(dist.Normal(1, 1), dist.Normal(mean, log_sd.exp()))
        (-model.prior.mu.log_prob(model.mu) + model.mu.log_prob(model.mu)).backward()
        self.assertIsNone(mean.grad)
        self.assertIsNone(log_sd.grad)

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        pass


class Test_PriorAsGuide(TestAutomaticGuide):
    _guide_cls = guide.PriorAsGuide

    def test_approximating_distribution_arguments_are_leaf_nodes(self):
        rv = RV(dist.Cauchy(torch.ones(3), torch.ones(3)))
        self.guide("rv2", rv)
        # todo: should this be a test for all guides?
        for rv in self.guide.random_variables():
            for param in rv.parameters():
                self.assertTrue(param.is_leaf)

    def test_observe_raises_userwarning(self):
        with self.assertWarns(UserWarning):
            self.guide.observe(rv=torch.ones(1))

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        pass


class TestPointMassGuide(TestAutomaticGuide):
    _guide_cls = guide.PointMassGuide

    def _draw_samples(self, n_samples=4):
        for _ in range(n_samples):
            self.guide.rsample()
            self.guide.save_state_as_sample()

    def test_added_random_variable_becomes_a_point_mass_rv(self):
        self.assertIsInstance(self.guide["rv"].distribution, dist.PointMass)

    def test_save_state_as_sample_calls_the_same_on_all_rvs(self):
        with mock.patch.object(
            self.guide["rv"].distribution, "save_state_as_sample"
        ) as mocked:
            self.guide.save_state_as_sample()
        mocked.assert_called()

    def test_save_state_as_sample(self):
        n_samples = 4
        # todo: mock the pointmass to set particular values
        for _ in range(n_samples):
            self.guide.rsample()
            self.guide.save_state_as_sample()
        self.assertEqual(self.guide._n_samples, n_samples)

    def test_sample_calls_sample_on_all_rvs(self):
        # todo: mock the pointmass to set particular values
        self._draw_samples()
        self.guide.sample()

    def test_returns_the_same_sample_when_called(self):
        self._draw_samples()
        self.guide.sample()  # sample selects a sample index from those stored
        sample1 = self.guide("rv", self.rv)
        sample2 = self.guide("rv", self.rv)
        self.assertTrue(torch.equal(sample1, sample2))

    def test_sample_with_no_added_rvars_raises_error(self):
        temp_guide = self._guide_cls()
        self.assertRaises(RuntimeError, temp_guide.sample)

    def test_sample_with_no_stored_samples(self):
        self.assertRaises(IndexError, self.guide.sample)

    def test_manually_specifying_q_dist(self):
        ## we dont expect this to work
        model = Module(guide=self._guide_cls())
        mean = torch.randn(1, requires_grad=True)
        log_sd = torch.randn(1, requires_grad=True)
        model.mu = RV(dist.Normal(1, 1), dist.Normal(mean, log_sd.exp()))
        self.assertIsInstance(model.mu.distribution, dist.PointMass)

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        pass


class TestManualGuide(TestAutomaticGuide):
    _guide_cls = guide.ManualGuide

    def test_rv_is_cloned_when_added(self):
        # This is NOT expected behaviour for the ManualGuide
        pass

    def test_getattr_returns_tensor_which_requires_grad(self):
        # NB this is only viable when the added random variable required grad
        rv = RV(dist.Normal(torch.ones(1, requires_grad=True), 1))
        self.guide["rv2"] = rv
        self.assertTrue(self.guide.rv2.requires_grad)

    def test_getattr_return_tensor_doesnt_require_grad_if_input_doesnt(self):
        self.assertFalse(self.guide.rv.requires_grad)

    def test_call_does_not_automatically_add_rvs(self):
        with self.assertRaises(AttributeError):
            self.guide("rv2", self.rv)

    def test_arguments_are_optimisable_for_added_rv(self):
        pass

    def test_create_ful_model(self):
        trace = self._guide_cls()
        model = Module(guide=trace)
        for _ in range(3):
            trace.par = Parameter(torch.ones(1))
            trace.mu = RV(dist.Normal(1, model.par))
            model.mu = RV(dist.Normal(0, 1))
            (-model.get_rv("mu").log_prob(model.mu)).backward()

    def test_manually_specifying_q_dist(self):
        ## we dont expect this to work
        trace = self._guide_cls()
        model = Module(guide=trace)
        mean = torch.randn(1, requires_grad=True)
        log_sd = torch.randn(1, requires_grad=True)
        trace.par = Parameter(torch.ones(1))
        trace.mu = RV(dist.Normal(1, model.par))
        model.mu = RV(dist.Normal(0, 1), dist.Normal(mean, log_sd.exp()))
        (-model.get_rv("mu").log_prob(model.mu)).backward()
        self.assertIsNone(mean.grad)

    def test_adding_rvs_in_no_grad_context_raises_warning(self):
        pass


class Test_set_guides(TestCase):
    def setUp(self):
        self.module = nn.Module(guide=guide.NormalGuide())
        self.module.lin = nn.Linear(10, 10)

    def test_set_guides_proppegate_to_children(self):
        self.assertIsInstance(self.module.lin.guide, guide.NormalGuide)
        self.assertIsInstance(self.module.guide, guide.NormalGuide)
        self.module.apply(guide.set_guides(guide.AutomaticGuide))
        self.assertIsInstance(self.module.lin.guide, guide.AutomaticGuide)
        self.assertIsInstance(self.module.guide, guide.AutomaticGuide)


if __name__ == "__main__":
    import unittest

    unittest.main()
