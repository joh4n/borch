from unittest import TestCase
import torch
from borch.distributions import constraints


class TestConstraint(TestCase):
    constraint = constraints.Constraint()
    valid = None
    invalid = None

    def test_check_valid_value_returns_true(self):
        if self.valid is not None:
            self.assertTrue(self.constraint.check(self.valid))

    def test_check_invalid_value(self):
        if self.invalid is not None:
            self.assertFalse(self.constraint.check(self.invalid))

    def test_constrinstrains_from_string(self):
        self.assertEqual(
            type(self.constraint),
            type(constraints.constraint_from_string(str(self.constraint))),
        )


class TestReal(TestConstraint):
    constraint = constraints.Real()
    valid = torch.randn(1)
    invalid = float("nan")


class TestDependent(TestConstraint):
    constraint = constraints.Dependent()

    def test_check_returns_valueerror(self):
        with self.assertRaises(ValueError):
            self.constraint.check(1)


class TestBoolean(TestConstraint):
    constraint = constraints.Boolean()
    valid = 1
    invalid = 10


class TestIntegerInterval(TestConstraint):
    constraint = constraints.IntegerInterval(1, 5)
    valid = 2
    invalid = 10


class TestIntegerLessThan(TestConstraint):
    constraint = constraints.IntegerLessThan(5)
    valid = 2
    invalid = 10


class TestIntegerGreaterThan(TestConstraint):
    constraint = constraints.IntegerGreaterThan(1)
    valid = 2
    invalid = -10


class TestGreaterThan(TestConstraint):
    constraint = constraints.GreaterThan(1)
    valid = 2
    invalid = 1


class TestGreaterThanEq(TestConstraint):
    constraint = constraints.GreaterThanEq(1)
    valid = 1
    invalid = -10


class TestPositive(TestConstraint):
    constraint = constraints.Positive()
    valid = 2
    invalid = -10


class TestLessThan(TestConstraint):
    constraint = constraints.LessThan(1)
    valid = -2
    invalid = 10


class TestInterval(TestConstraint):
    constraint = constraints.Interval(-3, 1)
    valid = 1
    invalid = 2


class TestHalfOpenInterval(TestConstraint):
    constraint = constraints.HalfOpenInterval(-3, 1)
    valid = -3
    invalid = 1


class TestSimplex(TestConstraint):
    constraint = constraints.Simplex()
    valid = torch.ones(1)
    invalid = torch.ones(2)


class TestLowerTriangular(TestConstraint):
    constraint = constraints.LowerTriangular()
    valid = torch.tensor([[1, 0, 0], [1, 1, 0], [1, 1, 1]])
    invalid = torch.ones(10, 10)


class TestLowerCholesky(TestConstraint):
    constraint = constraints.LowerTriangular()
    valid = torch.ones(10, 10).tril()
    invalid = torch.ones(10, 10)


class TestPositiveDefinite(TestConstraint):
    constraint = constraints.PositiveDefinite()
    valid = torch.tensor([[1.0, 0.0], [0.0, 1.0]])
    invalid = torch.ones(10, 10)


class TestRealVector(TestConstraint):
    constraint = constraints.RealVector()
    invalid = torch.tensor([1.0, float("nan")])
    valid = torch.ones(10)


class TestCat(TestConstraint):
    constraint = constraints.Cat([constraints.Real()])
    invalid = None
    valid = torch.ones(10)

    def test_constrinstrains_from_string(self):
        pass


class TestStack(TestConstraint):
    constraint = constraints.Stack([constraints.Real()])
    invalid = None
    valid = torch.ones(10)

    def test_constrinstrains_from_string(self):
        pass


class Test_is_dependet(TestCase):
    def test_called_with_dependet_returns_true(self):
        self.assertTrue(constraints.is_dependent(constraints.Dependent()))

    def test_called_with_real_returns_false(self):
        self.assertFalse(constraints.is_dependent(constraints.Real()))
