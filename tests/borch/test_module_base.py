import io
import unittest
from unittest import TestCase, mock

import torch

from borch import RV, TP, module_base
from borch.distributions import distributions

MODULE_PATH = "borch.module_base"


class TestModuleBase(TestCase):
    _cls = module_base.ModuleBase

    def setUp(self):
        self.module = self._cls()
        self.nested = self._cls()
        self.module.add_module("nested", self.nested)

        self.mu = TP(lambda x: x, torch.nn.Parameter(torch.Tensor([1, 2, 3.0])))
        self.sd = TP(lambda x: x, torch.nn.Parameter(torch.Tensor([1, 1, 1.0])))
        self.rv = RV(distributions.Normal(self.mu, self.sd))
        self.discrete_rv = RV(distributions.Poisson(3))

    def test_gettattr_for_observed_rv(self):
        obs = torch.ones(1)
        self.rv.observe(obs)
        self.module.rv = self.rv
        self.assertEqual(id(self.module.rv.observed), id(obs))

    def test_object_inherits_from_borch_module(self):
        self.assertIsInstance(self.module, module_base.ModuleBase)

    def test_cannot_register_random_variable_without_parameters_dict(self):
        del self.module._parameters
        with self.assertRaises(AttributeError):
            self.module.register_random_variable("rv", self.rv)

    def test_cannot_setattr_random_variable_without_parameters_dict(self):
        del self.module._parameters
        with self.assertRaises(AttributeError):
            self.module.rv = self.rv

    def test_cannot_register_random_variable_when_name_exists(self):
        with self.assertRaises(KeyError):
            self.module.register_random_variable("sample", self.rv)

    def test_cannot_register_random_variable_with_dot_in_name(self):
        with self.assertRaises(KeyError):
            self.module.register_random_variable("sa.mple", self.rv)

    def test_cannot_register_random_variable_with_no_name(self):
        with self.assertRaises(KeyError):
            self.module.register_random_variable("", self.rv)

    def test_cannot_register_random_variable_with_non_rv_object(self):
        with self.assertRaises(TypeError):
            self.module.register_random_variable("rv", self.mu)

    def test_sample_calls_rv_sample_for_discrete_distribution(self):
        self.module.rv = self.discrete_rv
        with mock.patch.object(self.discrete_rv, "sample") as mocked:
            self.module.sample()
        mocked.assert_called()

    def test_sample_calls_rv_rsample_for_continuous_distribution(self):
        self.module.rv = self.rv
        with mock.patch.object(self.rv, "rsample") as mocked:
            self.module.sample()
        mocked.assert_called()

    def test_getattr_calls_sample_on_discrete_rv_when_sampling(self):
        self.module.rv = self.discrete_rv
        self.module.sampling(True)
        with mock.patch.object(self.discrete_rv, "sample") as mocked:
            _ = self.module.rv
        mocked.assert_called()

    def test_getattr_calls_rsample_on_continuous_rv_when_sampling(self):
        self.module.rv = self.rv
        self.module.sampling(True)
        with mock.patch.object(self.rv, "rsample") as mocked:
            _ = self.module.rv
        mocked.assert_called()

    def test_sample_calls_sample_on_all_rvs_including_submodules(self):
        for rv in self.module.random_variables():
            rv.rsample = mock.create_autospec(rv.rsample)
            rv.sample = mock.create_autospec(rv.sample)

        self.module.sample()

        for rv in self.module.random_variables():
            self.assertTrue(rv.sample.called or rv.rsample.called)

    def test__apply_calls_function_on_all_tensor_base_parameters(self):
        func = mock.MagicMock(return_value=torch.Tensor([500.0]))
        self.module.rv = self.rv
        self.module.nested.rv2 = self.discrete_rv
        self.module.nested.param = torch.nn.Parameter(torch.Tensor([1.0]))

        self.module._apply(func)

        for item in self.module.parameters():
            self.assertTrue(torch.equal(item.data, func.return_value))

    def test__apply_calls_function_on_all_gradients(self):
        func = mock.MagicMock(return_value=torch.Tensor([500.0]))
        param = torch.nn.Parameter(torch.Tensor([1.0]))

        # assign gradients to all tensors / parameters
        param._grad = torch.Tensor([1.0])
        self.rv._grad = self.mu.data
        for param in self.rv.parameters():
            param._grad = self.mu.data

        self.module.rv = self.rv
        self.module.param = param
        self.module._apply(func)

        for item in self.module.parameters():
            if hasattr(item, "_grad") and item._grad is not None:
                self.assertTrue(torch.equal(item._grad, func.return_value))

    def test__apply_calls_function_on_all_buffers(self):
        func = mock.MagicMock(return_value=torch.Tensor([500.0]))

        # register a buffer
        self.module.register_buffer("test", torch.Tensor([1.0]))
        self.module._apply(func)

        for item in self.module.buffers():
            self.assertTrue(torch.equal(item.data, func.return_value))

    @unittest.skipUnless(torch.cuda.is_available(), "Only valid for cuda")
    def test_cuda_puts_all_parameters_on_cuda(self):
        self.module.rv = self.rv
        self.module.nested.rv2 = self.discrete_rv
        self.module.nested.param = torch.nn.Parameter(torch.Tensor([1.0]))
        self.module.cuda()
        for item in self.module.parameters():
            self.assertEqual(item.device.type, "cuda")

    def test_register_random_variable_adds_rv_to_parameters_dict(self):
        # the _parameters dict is used to contain random variables, HOWEVER
        # note that only the optimisable parameters themselves are returned by
        # `Module.parameters()` (which is usually not random variable objects)
        name = "test"
        self.module.register_random_variable(name, self.rv)
        self.assertIs(self.rv, self.module._parameters[name])

    def test_get_rv(self):
        self.module.rv = self.rv
        self.assertIs(self.module.get_rv("rv"), self.rv)

    def test_get_rv_that_is_not_added_raises_error(self):
        self.assertRaises(AttributeError, lambda: self.module.get_rv("rv"))

    def test_named_random_variables_yields_a_given_random_variable(self):
        self.module.register_random_variable("test", self.rv)
        rvs = map(lambda x: id(x[1]), self.module.named_random_variables())
        self.assertIn(id(self.rv), rvs)

    def test_parameters_yields_a_given_parameter(self):
        param = torch.nn.Parameter(torch.Tensor([3.0]))
        self.module.register_parameter("test", param)
        self.assertIn(param, self.module.parameters())

    def test_named_parameters_yields_a_given_parameter(self):
        param = torch.nn.Parameter(torch.Tensor([3.0]))
        self.module.register_parameter("test", param)
        all_pars = [par for _, par in self.module.named_parameters()]
        self.assertIn(param, all_pars)

    def test_named_parameters_with_torch_module_without_optimizable_par(self):
        param = torch.nn.Parameter(torch.Tensor([3.0]))
        self.module.register_parameter("test", param)
        self.module.lin = torch.nn.Linear(10, 10)
        for par in self.module.lin.parameters():
            par.requires_grad = False
        all_pars = [par for _, par in self.module.named_parameters()]
        self.assertEqual(len(all_pars), 3)

    def test_named_parameters_mix_tensor_transformed_param(self):
        mu = torch.tensor([1, 2, 3.0], requires_grad=True)
        sd = TP(lambda x: x.exp(), torch.nn.Parameter(torch.Tensor([1, 1, 1.0])))
        rv = RV(distributions.Normal(mu, sd))

        self.module.rv = rv
        observed = []
        for name, _ in self.module.named_parameters():
            observed.append(name)
        self.assertEqual(len(observed), 2)

    def test_opt_parameters_mix_with_and_without_grad(self):
        data = torch.Tensor([3.0])
        param = torch.nn.Parameter(data)
        self.module._parameters["data"] = data
        self.module.register_parameter("param", param)
        self.assertEqual(len(list(self.module.opt_parameters())), 1)

    def test_can_add_parameter_to_module(self):
        name = "param"
        setattr(self.module, name, torch.nn.Parameter(torch.ones(3)))
        self.assertIsInstance(getattr(self.module, name), torch.nn.Parameter)

    def test_random_variables_returns_all_random_variables(self):
        self.module.register_random_variable("test", self.rv)
        for rv in self.module.random_variables():
            self.assertIsInstance(rv, RV)

    def test_random_variables_returns_all_assigned_random_variables(self):
        self.module.rv = self.rv
        self.module.discrete_rv = self.discrete_rv
        self.assertTrue(len(tuple(self.module.random_variables())), 2)

    def test_getitem_fetches_correct_random_variable(self):
        self.module.rv = self.rv
        self.assertIs(self.module.get_rv("rv"), self.rv)

    def test_getitem_raises_attribute_error_if_nonexistant(self):
        self.module.rv = self.rv
        with self.assertRaises(AttributeError):
            _ = self.module["X"]

    def test_setitem_can_be_used_to_set_a_random_variable(self):
        self.module["rv"] = self.rv
        self.assertIs(self.module.get_rv("rv"), self.rv)

    def test_setitem_raises_value_error_if_not_a_parameter_or_rv(self):
        with self.assertRaises(ValueError):
            self.module["a"] = 1

    def test_remove_random_variables_removes_rvs_from_self_and_children(self):
        self.module.rv = self.rv  # add a random variable
        self.module.remove_random_variables()
        rvs = tuple(self.module.random_variables())
        self.assertFalse(bool(rvs))

    def test_getattr_observes_random_variable_when_observed_is_set(self):
        self.module.observe(test=torch.tensor([100.0]))
        self.module.test = self.rv
        with mock.patch.object(self.rv, "observe") as mocked:
            _ = self.module.test
        mocked.assert_called()

    def test_setting_observed_as_none_removes_all_observe_statuses(self):
        self.module.observe(test=torch.tensor([100.0]))
        self.module.observe(None)
        with mock.patch.object(self.rv, "observe") as mocked:
            self.module.test = self.rv
        mocked.assert_not_called()

    def test_setting_observed_kwarg_as_none_removes_observe_status(self):
        self.module.observe(test=torch.tensor([100.0]))
        self.module.observe(test=None)
        with mock.patch.object(self.rv, "observe") as mocked:
            self.module.test = self.rv
        mocked.assert_not_called()

    def test_observe_with_incorrect_call_pattern_raises_value_error(self):
        with self.assertRaises(ValueError):
            self.module.observe(None, None)

    def test_observe_with_non_tensor_object_raises_type_error(self):
        with self.assertRaises(TypeError):
            self.module.observe(rv=3)

    def test_rvs_on_noncontiguous_children_are_sampled(self):
        self.module.rv = self.rv
        self.module.discrete_rv = self.discrete_rv
        torch_mod = torch.nn.Sequential(self.module)

        mod = module_base.ModuleBase()
        mod.add_module("torch_mod", torch_mod)

        call_ids = []
        with mock.patch(
            f"{MODULE_PATH}.rsample_else_sample",
            side_effect=lambda x: call_ids.append(id(x)),
        ):
            mod.sample()

        for rv in self.module.random_variables():
            self.assertIn(id(rv), call_ids)


class Inheritor(module_base.ModuleBase):
    def __init__(self):
        super().__init__()
        self.fc1 = module_base.ModuleBase()
        self.disappearing = RV(distributions.Normal(1, 1))
        self.rv = None

    def forward(self, *inputs):  # pylint: disable=unused-argument
        self.rv = RV(distributions.Normal(1, 1))
        self.fc1.rv_linear = RV(distributions.Normal(1, 1))
        self.disappearing = None


class TestModuleBaseSerialisation(TestCase):
    def setUp(self):
        f = io.BytesIO()
        mod = Inheritor()
        mod(torch.rand(2, 10))  # perform a forward pass
        torch.save(mod.state_dict(), f)
        f.seek(0)

        self.mod = mod
        self.state_dict = torch.load(f)

    def test_using_torch_load_restores_all_random_variables(self):
        mod2 = Inheritor()
        mod2(torch.rand(2, 10))  # perform a forward pass
        mod2.load_state_dict(self.state_dict)

        self.assertIsInstance(mod2._parameters["rv"], RV)
        self.assertEqual(
            len(tuple(self.mod.random_variables())), len(tuple(mod2.random_variables()))
        )

    def test_strict_raises_a_runtime_error_for_unexpected_entries(self):
        mod2 = Inheritor()
        with self.assertRaises(RuntimeError):
            mod2.load_state_dict(self.state_dict, strict=True)

    def test_corrupted_tensor_in_state_dict_raises_runtime_error(self):
        mod2 = Inheritor()
        mod2.rv = torch.nn.Parameter(torch.Tensor([1.0]))
        self.state_dict["rv"] = torch.Tensor([2, 3, 4.0])  # alter the RV
        with self.assertRaises(RuntimeError):
            mod2.load_state_dict(self.state_dict)

    def test_non_rv_instance_is_set_using_setattr(self):
        mod2 = Inheritor()
        self.state_dict["hi"] = "hello"
        with mock.patch.object(Inheritor, "__setattr__") as mocked:
            mod2.load_state_dict(self.state_dict)
        mocked.assert_called_with("hi", "hello")


if __name__ == "__main__":
    unittest.main()
