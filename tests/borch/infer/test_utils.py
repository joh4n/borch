from unittest import TestCase

import torch
from torch.distributions.kl import _KL_REGISTRY

from borch.distributions import distributions
from borch.infer import utils


class TestKlExists(TestCase):
    def test_correctness_for_existence(self):
        norm = distributions.Normal(torch.tensor(1.0), torch.tensor(2.0))
        self.assertTrue(utils.kl_exists(norm, norm))

    def test_correctness_for_nonexistence(self):
        norm = distributions.Normal(torch.tensor(1.0), torch.tensor(2.0))
        cat = distributions.Categorical(probs=torch.tensor([0.5, 0.5]))
        key = (type(norm), type(cat))
        assert _KL_REGISTRY.get(key) is None, "Update test"
        self.assertFalse(utils.kl_exists(norm, cat))
