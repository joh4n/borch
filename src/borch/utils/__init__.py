"""
Utility functions
=================
Mix of utility functions used in various places of ppl. The functions are separated
into sub modules with minimal dependencies.


NOTE:
The utils package is not allowed to include imports from other parts of ppl. Any
function that depends on an import from `ppl.*.**` where * is not utils should be
placed in an appropriate `ppl.*.utils` file.

"""
