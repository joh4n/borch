FROM ubuntu:18.04

ARG ARCH
RUN test -n "$ARCH"  # --build-arg ARCH must be set

ARG CI_COMMIT_REF_NAME

COPY scripts/install-python.sh .

RUN apt update -q && \
    apt install -yq --no-install-recommends \
      build-essential \
      ca-certificates \
      curl \
      git \
      gnupg \
      libglib2.0-0 && \
    bash install-python.sh && \
    rm -rf install-python.sh /var/lib/apt/lists/*

WORKDIR /borch

ENV LC_ALL="C.UTF-8" \
    LANG="C.UTF-8"

# Install borch
COPY . .
RUN make install
