from unittest import TestCase, mock

import numpy as np
import torch
from torch import Tensor

from borch import RV, rv_factories
from borch import distributions as dist


class TestParameterToNormalRV(TestCase):
    def setUp(self):
        self.shape = (1, 2, 3)
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = rv_factories.parameter_to_normal_rv("param", self.param)

    def assertEqual(self, first, second, msg=None):  # noqa: N802
        if isinstance(first, Tensor) and isinstance(second, Tensor):
            self.assertTrue(torch.equal(first, second))
        else:
            super().assertEqual(first, second, msg)

    def test_values_of_arguments_are_correctly_set(self):
        values = {"prior_mean": 3, "prior_sd": 100}
        rv = rv_factories.parameter_to_normal_rv("param", self.param, **values)

        def _create_tensor(name):
            return torch.ones(self.shape) * values[name]

        self.assertEqual(rv.loc, _create_tensor("prior_mean"))
        self.assertEqual(rv.scale, _create_tensor("prior_sd"))

    def test_returned_object_is_random_variable(self):
        self.assertIsInstance(self.rv, RV)

    def test_rv_data_is_the_same_as_parameter_data(self):
        self.assertEqual(self.rv.data, self.param.data)

    def test_priors_dont_require_gradients_when_set_with_numbers(self):
        rv = rv_factories.parameter_to_normal_rv(
            "param", self.param, prior_mean=1, prior_sd=1
        )
        self.assertFalse(rv.distribution.loc.requires_grad)
        self.assertFalse(rv.distribution.scale.requires_grad)

    def test_priors_require_gradients_when_set_with_such_tensors(self):
        p_mean = torch.ones(*self.shape, requires_grad=True)
        p_sd = torch.ones(*self.shape, requires_grad=True)
        rv = rv_factories.parameter_to_normal_rv(
            "param", self.param, prior_mean=p_mean, prior_sd=p_sd
        )
        self.assertTrue(rv.distribution.loc.requires_grad)
        self.assertTrue(rv.distribution.scale.requires_grad)


class Test_parameter_to_scaled_normal_rv(TestCase):
    rv_factory = staticmethod(rv_factories.parameter_to_scaled_normal_rv)
    scale = 1

    def setUp(self):
        self.shape = (1, 2, 3)
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = self.rv_factory("param", self.param)

    def test_correct_scale(self):
        self.assertAlmostEqual(self.rv.scale.mean().item(), self.scale)

    def test_correct_loc(self):
        self.assertAlmostEqual(self.rv.loc.mean().item(), 0)

    def test_correct_distribution(self):
        self.assertIsInstance(self.rv.distribution, dist.Normal)

    def test_returns_random_variable(self):
        self.assertIsInstance(self.rv, RV)

    def test_dist_args_is_leaf(self):
        self.assertTrue(self.rv.distribution.loc.is_leaf)
        self.assertTrue(self.rv.distribution.scale.is_leaf)

    def test_dist_args_requires_no_grad(self):
        self.assertFalse(self.rv.distribution.loc.requires_grad)
        self.assertFalse(self.rv.distribution.scale.requires_grad)


class Test_kaiming_normal_rv(Test_parameter_to_scaled_normal_rv):
    rv_factory = staticmethod(rv_factories.kaiming_normal_rv)
    scale = 0.40824833512306213


class Test_xavier_normal_rv(Test_parameter_to_scaled_normal_rv):
    rv_factory = staticmethod(rv_factories.xavier_normal_rv)
    scale = 0.4714045226573944


class TestApplyRVFactory(TestCase):
    def setUp(self):
        self.module = torch.nn.Module()
        self.param = torch.nn.Parameter(torch.ones(3))
        self.module.param = self.param

    @mock.patch("torch.nn.Module.__setattr__")
    def test_setattr_called_with_param_as_rv(self, mocked):
        rv_factories.apply_rv_factory(self.module, rv_factories.parameter_to_normal_rv)
        mocked.assert_called()
        call_args = mocked.call_args[0]  # check the first call of setattr
        self.assertEqual(call_args[0], "param")  # name should have been param
        self.assertIsInstance(call_args[1], RV)  # attr should have been an RV


class Test_priors_to_rv(TestCase):
    rv_factory = staticmethod(rv_factories.priors_to_rv)

    def setUp(self):
        self.shape = (3, 2, 3)
        self.loc = torch.randn(1, requires_grad=True)
        self.pdist = dist.Normal(self.loc, 10)
        self.priors = {"param": self.pdist}
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = self.rv_factory(
            "param", self.param, self.priors, rv_factories.parameter_to_normal_rv
        )

    @mock.patch.object(rv_factories, "parameter_to_normal_rv")
    def test_uses_propper_fallback(self, mocked):
        self.rv_factory("param", self.param, {}, rv_factories.parameter_to_normal_rv)
        mocked.assert_called()

    def test_returns_param_if_no_fallback(self):
        returned_param = self.rv_factory("param", self.param, {}, None)
        self.assertNotIsInstance(returned_param, RV)
        self.assertTrue(torch.eq(returned_param, self.param).all())

    def test_correct_scale(self):
        self.assertAlmostEqual(
            self.rv.scale.mean().item(), self.pdist.scale.mean().item()
        )

    def test_correct_loc(self):
        self.assertAlmostEqual(
            self.rv.loc.mean().item(), self.pdist.loc.mean().item(), delta=1e-6
        )

    def test_correct_distribution(self):
        self.assertIsInstance(self.rv.distribution, dist.Normal)

    def test_returns_random_variable(self):
        self.assertIsInstance(self.rv, RV)

    def test_dist_args_is_leaf(self):
        self.assertFalse(self.rv.distribution.loc.is_leaf)
        self.assertTrue(self.rv.distribution.scale.is_leaf)

    def test_dist_args_requires_no_grad(self):
        self.assertTrue(self.rv.distribution.loc.requires_grad)
        self.assertFalse(self.rv.distribution.scale.requires_grad)

    def test_backward_once(self):
        self.rv.exp().sum().backward()
        self.assertIsNotNone(self.loc.grad)

    def test_backward_several_times(self):
        self.rv.rsample()
        self.rv.exp().sum().backward()
        self.rv.rsample()
        self.rv.exp().sum().backward()
        self.assertIsNotNone(self.loc.grad)


class Test_infinitely_wide_rv(TestCase):
    def setUp(self):
        self.parameter = torch.randn(3, 3, 3)
        self.infinite_rv = rv_factories.parameter_to_maxwidth_uniform(
            "name", self.parameter
        )

    def test_all_values_equally_probable(self):
        ones = torch.ones(3, 3, 3)

        self.assertNotEqual(ones.mean(), self.parameter.mean())

        mean1 = self.infinite_rv.distribution.log_prob(self.parameter).mean()
        mean2 = self.infinite_rv.distribution.log_prob(ones).mean()

        self.assertEqual(mean1, mean2)

    def test_var_is_inf(self):
        self.assertEqual(self.infinite_rv.sample().var().item(), np.float("inf"))


class Test_delta_rv(TestCase):
    def setUp(self):
        self.parameter = torch.randn(3, 3, 3)
        self.delta_rv = rv_factories.delta_rv("name", self.parameter)

    def test_is_rv(self):
        self.assertIsInstance(self.delta_rv, RV)

    def test_returns_right_value(self):
        self.assertEqual(self.delta_rv.sample().sum(), self.parameter.sum())


if __name__ == "__main__":
    import unittest

    unittest.main()
