from unittest import TestCase

import torch
from torch import nn

from borch.nn import Linear
from borch.utils import module_utils


class TestTotalParameters(TestCase):
    def setUp(self):
        self.net = torch.nn.Sequential(
            torch.nn.Linear(4, 5, bias=True),
            torch.nn.Sigmoid(),
            torch.nn.Linear(5, 6, bias=True),
            torch.nn.Sigmoid(),
            torch.nn.Linear(6, 7, bias=True),
            torch.nn.Sigmoid(),
        )

    def test_correctness_of_total_params(self):
        # From the network above we have the following number of trainable
        # parameters (including biases!):
        layer_1 = (4 + 1) * 5
        layer_2 = (5 + 1) * 6
        layer_3 = (6 + 1) * 7
        truth = layer_1 + layer_2 + layer_3
        self.assertEqual(module_utils.total_parameters(self.net), truth)


class TestCopyModuleAttributes(TestCase):
    def setUp(self):
        self.original = torch.nn.Linear(3, 4)
        self.copy = torch.nn.Linear(3, 4)

    def test_paramters_retain_their_class(self):
        module_utils.copy_module_attributes(self.original, self.copy)
        self.assertIsInstance(self.copy.weight, torch.nn.Parameter)

    def test_paramters_retain_their_attributes(self):
        name = "something"
        setattr(self.original.weight, name, 1)
        module_utils.copy_module_attributes(self.original, self.copy)
        self.assertTrue(hasattr(self.copy.weight, name))

    def test_copy_retains_attributes_from_original(self):
        name = "something"
        setattr(self.original, name, 2)
        module_utils.copy_module_attributes(self.original, self.copy)
        self.assertTrue(hasattr(self.copy, name))

    def test_copy_parameters_are_unique_objects(self):
        module_utils.copy_module_attributes(self.original, self.copy)
        self.assertIsNot(self.original.weight, self.copy.weight)


class Test_parameters_not_named(TestCase):
    def test_one_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(len(list(module_utils.parameters_not_named(net, "bad"))), 2)

    def test_nothing_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(len(list(module_utils.parameters_not_named(net, "bad"))), 3)

    def test_remove_scale(self):
        net = Linear(2, 2)
        self.assertEqual(
            len(list(module_utils.parameters_not_named(net, "scale.u_tensor"))), 6
        )


class Test_parameters_named(TestCase):
    def test_one_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(len(list(module_utils.parameters_named(net, "bad"))), 1)

    def test_nothing_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(len(list(module_utils.parameters_named(net, "bad"))), 0)

    def test_get_scale(self):
        net = Linear(2, 2)
        self.assertEqual(
            len(list(module_utils.parameters_named(net, "scale.u_tensor"))), 2
        )


class Test_yield_not_named(TestCase):
    def test_one_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(
            len(list(module_utils.yield_not_named(net.named_parameters(), "bad"))), 2
        )

    def test_nothing_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(
            len(list(module_utils.yield_not_named(net.named_parameters(), "bad"))), 3
        )

    def test_remove_scale(self):
        net = Linear(2, 2)
        self.assertEqual(
            len(
                list(
                    module_utils.yield_not_named(
                        net.named_parameters(), "scale.u_tensor"
                    )
                )
            ),
            6,
        )


class Test_yield_named(TestCase):
    def test_one_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(
            len(list(module_utils.yield_named(net.named_parameters(), "bad"))), 1
        )

    def test_nothing_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(
            len(list(module_utils.yield_named(net.named_parameters(), "bad"))), 0
        )

    def test_get_scale(self):
        net = Linear(2, 2)
        self.assertEqual(
            len(
                list(
                    module_utils.yield_not_named(
                        net.named_parameters(), "scale.u_tensor"
                    )
                )
            ),
            6,
        )


class Test_yield_named_opt_param(TestCase):
    def test_returns_correct_num_params_for_linear(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(
            len(list(module_utils.yield_named_opt_param(net.named_parameters()))), 3
        )

    def test_nothing_to_return(self):
        params = [(str(i), torch.ones(1, requires_grad=False)) for i in range(4)]
        self.assertEqual(len(list(module_utils.yield_named_opt_param(params))), 0)


class Test_yield_opt_param(TestCase):
    def test_returns_correct_num_params_for_linear(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        self.assertEqual(len(list(module_utils.yield_opt_param(net.parameters()))), 3)

    def test_nothing_to_return(self):
        params = [torch.ones(1, requires_grad=False) for i in range(4)]
        self.assertEqual(len(list(module_utils.yield_opt_param(params))), 0)


def test_get_nested_module():
    expected = nn.Linear(3, 4)
    net = nn.Sequential(
        nn.Sequential(nn.Linear(2, 3), expected),
        nn.Sequential(nn.Linear(4, 5), nn.Linear(5, 6)),
    )
    response = module_utils.get_nested_module(net, ("0", "1"))
    assert response is expected


def test_get_nested_modules():
    expected = (nn.Linear(3, 4), nn.Linear(4, 5))
    net = nn.Sequential(
        nn.Sequential(nn.Linear(2, 3), expected[0]),
        nn.Sequential(expected[1], nn.Linear(5, 6)),
    )
    response = module_utils.get_nested_modules(net, [("0", "1"), ("1", "0")])
    assert response == expected


class Network(nn.Sequential):
    def __init__(self, n_out):
        super().__init__()
        self.one = nn.Linear(3, 4)
        self.two = nn.Linear(4, 5)
        self.thr = nn.Linear(5, n_out)


class TestLoadStateDict(TestCase):
    def setUp(self):
        self.net = Network(n_out=10)
        self.state_dict = self.net.state_dict()

    def test_raises_errors_when_names_mismatched_and_strict_names(self):
        name, tensor = self.state_dict.popitem()
        self.state_dict[name + "2"] = tensor

        with self.assertRaises(RuntimeError):
            # This is the basic PyTorch behaviour
            module_utils.load_state_dict(self.net, self.state_dict)

        with self.assertRaises(RuntimeError):
            # This is the custom behaviour
            module_utils.load_state_dict(self.net, self.state_dict, strict_shapes=False)

    def test_when_names_mismatched_and_strict_names_is_false(self):
        name, tensor = self.state_dict.popitem()
        self.state_dict[name + "2"] = tensor
        module_utils.load_state_dict(self.net, self.state_dict, strict_names=False)
        module_utils.load_state_dict(
            self.net, self.state_dict, strict_names=False, strict_shapes=False
        )

    def test_loads_dict_with_mismatched_shapes(self):
        net = Network(n_out=20)
        module_utils.load_state_dict(net, self.state_dict, strict_shapes=False)

    def test_loads_dict_with_mismatched_names_when_strict_shapes_is_false(self):
        _, tensor = self.state_dict.popitem()
        self.state_dict["hello.weight"] = tensor
        module_utils.load_state_dict(
            self.net, self.state_dict, strict_names=False, strict_shapes=False
        )
