"""
This module contains activation functions for neural networks.
"""

from borch.nn.activations.activation_functions import Hsigmoid, Hswish
