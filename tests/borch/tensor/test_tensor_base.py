from io import BytesIO
import pickle as pkl
from types import GeneratorType
import unittest

import numpy as np
import torch

from borch.tensor import tensor_base
from borch.utils.torch_utils import get_device

DEVICE = get_device()


class Inheritor(tensor_base.TensorBase):
    def rsample(self):
        pass

    def recalculate(self):
        pass


class StochasticInheritor(tensor_base.TensorBase):
    def __init__(self):
        super().__init__()
        self.u_tensor = torch.ones(2)
        self.recalculate()

    def recalculate(self):
        self.data = self.transform(self.u_tensor)

    @staticmethod
    def transform(x):
        return torch.randn(x.shape) + x


class InheritorWithTensorBaseChild(Inheritor):
    inheritor = Inheritor()

    def parameters(self, seen=None):
        yield self.inheritor


class TestTensorBase(unittest.TestCase):
    def setUp(self):
        self.tensor_base_inheritor = Inheritor()

    def _generate_paramaters(self):
        for i in range(3):
            self.tensor_base_inheritor._parameters[i] = torch.ones(
                i, requires_grad=True
            )
            self.tensor_base_inheritor._parameters[f"{i}"] = torch.ones(i)
        self._add_child_with_parameters()

    def _add_child_with_parameters(self):
        new = Inheritor()
        for i in range(3):
            new._parameters[i] = torch.ones(i, requires_grad=True)
            new._parameters[f"{i}"] = torch.ones(i)
        self.tensor_base_inheritor._parameters["child"] = new

    def _parameters_are_type(self, dtype):
        for val in self.tensor_base_inheritor.parameters():
            self.assertEqual(val.dtype, dtype)

    def test_correctness_of_type(self):
        self.assertIsInstance(self.tensor_base_inheritor, tensor_base.TensorBase)

    def test_repr(self):
        prefix = "TensorBase containing:\n"
        self.assertTrue(repr(tensor_base.TensorBase().fill_(1.0)).startswith(prefix))

    def test_correctness_of_clone_type(self):
        tensor = self.tensor_base_inheritor.clone()
        self.assertIsInstance(tensor, tensor_base.TensorBase)

    def test_correctness_of_expand_type(self):
        tensor = self.tensor_base_inheritor.expand(10, 10)
        self.assertIsInstance(tensor, tensor_base.TensorBase)

    def test_size_of_expanded_type(self):
        tensor = self.tensor_base_inheritor.expand(10, 10)
        self.assertEqual(tensor.size(), (10, 10))

    def test_correctness_of_view_type(self):
        tensor = self.tensor_base_inheritor.view(1, 1, 1)
        self.assertIsInstance(tensor, tensor_base.TensorBase)

    def test_size_of_view_type(self):
        tensor = self.tensor_base_inheritor.view(1, 1, 1)
        self.assertEqual(tensor.size(), (1, 1, 1))

    def test_parameters_are_retained_after_clone(self):
        self.tensor_base_inheritor._parameters["hi"] = 10
        tensor = self.tensor_base_inheritor.clone()
        self.assertIn("hi", tensor._parameters)

    def test_methods_are_retained_after_clone(self):
        tensor = Inheritor()
        new_tensor = tensor.clone()
        self.assertTrue(hasattr(new_tensor, "rsample"))

    def test_parameters_returns_all_expected_parameters(self):
        for i in range(3):
            self.tensor_base_inheritor._parameters[i] = torch.ones(
                i, requires_grad=True
            )
            self.tensor_base_inheritor._parameters[f"{i}"] = torch.ones(i)
        self.assertEqual(len(tuple(self.tensor_base_inheritor.parameters())), 6)

    def test_parameters_returns_only_unique_tensors(self):
        for i in range(3):
            tensor = torch.ones(i, requires_grad=True)
            self.tensor_base_inheritor._parameters[i] = tensor
            self.tensor_base_inheritor._parameters[i + 3] = tensor
        self.assertEqual(len(tuple(self.tensor_base_inheritor.parameters())), 3)

    def test_parameters_returns_nested_tensors(self):
        nested = tensor_base.TensorBase()
        for i in range(3):
            self.tensor_base_inheritor._parameters[i] = torch.ones(
                i, requires_grad=True
            )
            nested._parameters[i] = torch.ones(i, requires_grad=True)
        self.tensor_base_inheritor._parameters["nested"] = nested
        self.assertEqual(len(tuple(self.tensor_base_inheritor.parameters())), 6)

    def test_parameters_returns_only_unique_nested_tensors(self):
        nested = tensor_base.TensorBase()
        for i in range(3):
            tensor = torch.ones(i, requires_grad=True)
            self.tensor_base_inheritor._parameters[i] = tensor
            nested._parameters[i] = tensor
        self.tensor_base_inheritor._parameters["nested"] = nested
        self.assertEqual(len(tuple(self.tensor_base_inheritor.parameters())), 3)

    def test_ipow(self):
        with self.assertRaises(RuntimeError):
            self.tensor_base_inheritor.__ipow__(self.tensor_base_inheritor)

    def test_named_parameters_returns_all_expected_names_and_parameters(self):
        for i in range(3):
            self.tensor_base_inheritor._parameters[f"{i}"] = torch.ones(
                i + 1, requires_grad=True
            )
            self.tensor_base_inheritor._parameters[f"par{i}"] = torch.ones(i + 1)

        for name, tens in self.tensor_base_inheritor.named_parameters():
            self.assertTrue(name in self.tensor_base_inheritor._parameters)
            self.assertEqual(id(self.tensor_base_inheritor._parameters[name]), id(tens))

    def test_double(self):
        self._generate_paramaters()
        self.tensor_base_inheritor.double()
        self._parameters_are_type(torch.float64)

    def test_double_applies_to_selfs_self(self):
        self.assertEqual(self.tensor_base_inheritor.double().dtype, torch.float64)

    def test_double_propagates_to_grad(self):
        for i in range(3):
            self.tensor_base_inheritor._parameters[i] = torch.randn(
                1, requires_grad=True
            )
        sum(
            val ** 2 for val in self.tensor_base_inheritor._parameters.values()
        ).backward()
        self.tensor_base_inheritor.double()
        self._parameters_are_type(torch.float64)

    def test_half(self):
        self._generate_paramaters()
        self.tensor_base_inheritor.half()
        self._parameters_are_type(torch.float16)

    def test_half_on_self(self):
        self.assertEqual(self.tensor_base_inheritor.half().dtype, torch.float16)

    def test_correct_num_param_with_a_tensorbase_param(self):
        self._generate_paramaters()
        self.assertEqual(len(list(self.tensor_base_inheritor.parameters())), 12)

    def test_float(self):
        self._generate_paramaters()
        self.tensor_base_inheritor.half()
        self.tensor_base_inheritor.float()
        self._parameters_are_type(torch.float32)

    def test_float_on_self(self):
        self.tensor_base_inheritor.half()
        self.assertEqual(self.tensor_base_inheritor.float().dtype, torch.float32)

    def test_to(self):
        self._generate_paramaters()
        self.tensor_base_inheritor.to(torch.float16)
        self._parameters_are_type(torch.float16)

    def test_to_applies_to_self(self):
        self.assertEqual(
            self.tensor_base_inheritor.to(torch.float16).dtype, torch.float16
        )

    def test_type(self):
        self._generate_paramaters()
        self.tensor_base_inheritor.type(torch.float16)
        self._parameters_are_type(torch.float16)

    def test_type_applies_to_self(self):
        self.assertEqual(
            self.tensor_base_inheritor.type(torch.float16).dtype, torch.float16
        )

    def test_cuda(self):
        if torch.cuda.is_available():
            self.tensor_base_inheritor.cuda()
            for val in self.tensor_base_inheritor._parameters.values():
                self.assertIsInstance(val.get_device(), int)

    def test_cpu(self):
        if torch.cuda.is_available():
            self.tensor_base_inheritor.cuda()
            self.tensor_base_inheritor.cpu()
            for val in self.tensor_base_inheritor._parameters.values():
                self.assertEqual(val.device, torch.device("cpu"))

    def test_inheritor_class_after_pickle(self):
        f = BytesIO()
        pkl.dump(self.tensor_base_inheritor, f)
        f.seek(0)
        inheritor = pkl.load(f)
        self.assertIsInstance(inheritor, Inheritor)

    def test_inheritor_params_after_pickle(self):
        f = BytesIO()
        self.tensor_base_inheritor._parameters = {
            i: torch.randn(3, 3) for i in range(4)
        }
        pkl.dump(self.tensor_base_inheritor, f)
        f.seek(0)
        tensor_base_inheritor = pkl.load(f)

        for par1, par2 in zip(
            tensor_base_inheritor.parameters(), self.tensor_base_inheritor.parameters()
        ):
            np.testing.assert_equal(par1.detach().numpy(), par2.detach().numpy())

    def test_inheritor_has_correct_number_of_params_after_pickle(self):
        f = BytesIO()
        self.tensor_base_inheritor._parameters = {i: torch.ones(1) for i in range(10)}
        pkl.dump(self.tensor_base_inheritor, f)
        f.seek(0)
        tensor_base_inheritor = pkl.load(f)
        self.assertEqual(len(list(tensor_base_inheritor.parameters())), 10)

    def test_value_stays_the_same(self):
        f = BytesIO()
        pkl.dump(self.tensor_base_inheritor, f)
        f.seek(0)
        tensor_base_inheritor = pkl.load(f)
        np.testing.assert_equal(
            self.tensor_base_inheritor.detach().numpy(),
            tensor_base_inheritor.detach().numpy(),
        )

    @staticmethod
    def test_value_same_in_stochasic_inheritor():
        stoch_inheritor = StochasticInheritor()
        f = BytesIO()
        pkl.dump(stoch_inheritor, f)
        f.seek(0)
        pickled_stoch_inheritor = pkl.load(f)

        np.testing.assert_equal(
            stoch_inheritor.detach().numpy(), pickled_stoch_inheritor.detach().numpy()
        )

    def test_requires_grad_when_set_value_requires_grad(self):
        value = torch.ones(1, requires_grad=True)
        self.tensor_base_inheritor.set_value_(value)
        self.assertTrue(self.tensor_base_inheritor.requires_grad)

    def test_doesnt_requires_grad_when_set_value_doesnt_requires_grad(self):
        value = torch.ones(1)
        self.tensor_base_inheritor.set_value_(value)
        self.assertFalse(self.tensor_base_inheritor.requires_grad)

    def test_has_correct_value(self):
        value = torch.randn(1)
        detached = value.data.detach().clone().numpy()
        self.tensor_base_inheritor.set_value_(value)
        np.testing.assert_equal(self.tensor_base_inheritor.detach().numpy(), detached)

    def test_has_correct_data_type(self):
        value = torch.randn(1)
        detached = value.data.detach().clone().numpy()
        self.tensor_base_inheritor.set_value_(value)
        self.assertEqual(
            self.tensor_base_inheritor.detach().numpy().dtype, detached.dtype
        )

    def test__parameters_is_dict(self):
        self.assertIsInstance(self.tensor_base_inheritor._parameters, dict)

    def test_named_parameters_is_generator(self):
        self.assertIsInstance(
            self.tensor_base_inheritor.named_parameters(), GeneratorType
        )

    def test_parameters_is_generator(self):
        self.assertIsInstance(self.tensor_base_inheritor.parameters(), GeneratorType)

    def test__apply_recurse(self):
        self._add_child_with_parameters()
        new = self.tensor_base_inheritor.to(torch.float16)
        for par in new.parameters():
            self.assertEqual(par.dtype, torch.float16)

    def test_apply_recurse(self):
        with_child = InheritorWithTensorBaseChild()
        with_child.to(torch.float16)
        self.assertEqual(with_child.inheritor.dtype, torch.float16)

    def test_named_parameters_recurse_properly(self):
        self._add_child_with_parameters()
        seen = set()

        for _, _ in self.tensor_base_inheritor.named_parameters(seen=seen):
            pass

        self.assertIn(self.tensor_base_inheritor._parameters["child"], seen)
        for _, par in self.tensor_base_inheritor._parameters[
            "child"
        ]._parameters.items():
            self.assertIn(par, seen)
